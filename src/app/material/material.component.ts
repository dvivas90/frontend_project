import { Component, OnInit,ViewChild, ElementRef} from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import FileSaver from 'file-saver';
import { RestService} from '../providers/rest.service';
import * as XLSX from 'xlsx';

class Material {
  constructor(
    public idMATERIAL: string = '',
    public nomMATERIAL: string = '',
    public desMATERIAL: string = ''
  ) {}
}

@Component({
  selector: 'app-material',
  templateUrl: './material.component.html',
  styleUrls: ['./material.component.css']
})
export class MaterialComponent implements OnInit {
  formMaterial: FormGroup;
  @ViewChild('modalClose') modalClose:ElementRef;
  
  // It maintains list of materiales
  materiales: Material[] = [];
  // It maintains material Model
  regModel: Material;
  // It maintains material form display status. By default it will be false.
  showNew: Boolean = false;
  // It will be either 'Save' or 'Update' based on operation.
  submitType: string = 'Save';
  // It maintains table row index based on selection.
  selectedRow: number;
  // It maintains Array of countries.
  totalRec : number;
  page: number = 1;

  selectedIndex : number;

  public filtroNombre: String = null;

  constructor(private restService:RestService) {
    
    this.consultarMateriales();

  }

  consultarMateriales(){
    // Add default material data.

    this.materiales = [];

    this.restService.
      consultarMateriales()
        .subscribe(data => {
          if (data.success === true) {

            var arrayMateriales = [];
            arrayMateriales = data.materiales;

            this.totalRec = arrayMateriales.length;

            for (let material of arrayMateriales) {
              console.log("MATERIAL ENCONTRADA",material);
              this.materiales.push(new Material(material.idMATERIAL,material.nomMATERIAL, material.desMATERIAL));
           }
          
  
          } else {
            console.log("ERROR AL OBTENER MATERIALS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });
  }

  ngOnInit() {
    this.formMaterial = new FormGroup({
      'idMATERIAL': new FormControl({value:null,disabled: true}),
      'nomMATERIAL': new FormControl(null, Validators.compose([Validators.required,Validators.pattern(".*[^ ].*")])), 
      'desMATERIAL': new FormControl(null, Validators.compose([Validators.required,Validators.pattern(".*[^ ].*")]))
		});
    
  }

  // This method associate to New Button.
  onNew() {
    // Initiate new material.
    this.regModel = new Material();
    this.formMaterial.reset();
    // Change submitType to 'Save'.
    this.submitType = 'Save';
    // display material entry section.
    this.showNew = true;
  }

  filtrar(){
    var date = new Date();

    if (this.filtroNombre!=null && this.filtroNombre!=''){

      var filtros = { 
          nomMATERIAL:this.filtroNombre

      }

      this.restService.
       consultarMaterialesFiltros(filtros)
        .subscribe(data => {
          if (data.success === true) {

            this.materiales = data.materiales;

            this.totalRec = this.materiales.length;
          
          } else {
            console.log("ERROR AL CONSULTAR MATERIALS FILTROS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });

    }else{
      this.consultarMateriales();
    }

    
  
  }

  // This method associate to Save Button.
  onSave() {
    
    if(this.formMaterial.valid){

    if (this.submitType === 'Save') {
      // Push material model object into material list.

      var material = {
        nomMATERIAL:this.regModel.nomMATERIAL.trim(),
        desMATERIAL:this.regModel.desMATERIAL.trim()
      }
      this.restService.
       insertarMaterial(material)
        .subscribe(data => {
          if (data.success === true) {

            this.materiales.push(data.material);
          
          } else {
            console.log("ERROR AL INSERTAR MATERIALS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });


    } else {
      // Update the existing properties values based on model.

      this.restService.
       editarMaterial(this.regModel.idMATERIAL,this.regModel)
        .subscribe(data => {
          if (data.success === true) {
          this.materiales[this.selectedRow].idMATERIAL = this.regModel.idMATERIAL;
          this.materiales[this.selectedRow].nomMATERIAL = this.regModel.nomMATERIAL;
          this.materiales[this.selectedRow].desMATERIAL = this.regModel.desMATERIAL;
          } else {
            console.log("ERROR AL EDITAR MATERIALS");
          }
        });
    }
    // Hide material entry section.
    this.showNew = false;
    this.modalClose.nativeElement.click();
    }
  }

  // This method associate to Edit Button.
  onEdit(index: number) {
    // Assign selected table row index.
    this.selectedRow = index;
    // Initiate new material.
    this.regModel = new Material();

    console.log('MATERIAL EDITAR',this.materiales[this.selectedRow])
    // Retrieve selected material from list and assign to model.
    this.regModel = Object.assign({}, this.materiales[this.selectedRow]);
    // Change submitType to Update.
    this.submitType = 'Update';
    // Display material entry section.
    this.showNew = true;
  }

  // This method associate to Delete Button.
  confirmDelete(index: number) {
    console.log("AQUI CONFIRMAR ELIMINACION",index);
    this.selectedIndex = index;
  }

  // This method associate to Delete Button.
  onDelete(index: number) {
    // Delete the corresponding material entry from the list.
    console.log("AQUI CONFIRMAR ELIMINACION",index);
    this.restService.
      eliminarMaterial(this.materiales[index].idMATERIAL)
        .subscribe(data => {
          if (data.success === true) {
            this.materiales.splice(index,1);
          } else {
            console.log("ERROR AL ELIMINAR MATERIALS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });
    
    
  }

  // This method associate toCancel Button.
  onCancel() {
    // Hide material entry section.
    this.showNew = false;
  }

 

  exportarExcel(){

    /* make the worksheet */
    var ws = XLSX.utils.json_to_sheet(this.materiales);

    /* add to workbook */
    var wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "People");

    /* write workbook (use type 'binary') */
    var wbout = XLSX.write(wb, {bookType:'xlsx', type:'binary'});

    var today = new Date();
    var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
    var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
    var name = "reporteMaterial" + date + time;

    FileSaver.saveAs(new Blob([this.s2ab(wbout)],{type:"application/octet-stream"}), name+".xlsx");
  }

   s2ab(s) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
  }

   exportAsExcelFile(json: any[], excelFileName: string): void {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }

    private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
          type: "application/octet-stream"
        });
        var today = new Date();
        var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
        var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
        var name = fileName + date + time;
        FileSaver.saveAs(data, name + ".xlsx");
}




}
