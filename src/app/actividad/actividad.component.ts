import { Component, OnInit,ViewChild, ElementRef} from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import FileSaver from 'file-saver';
import { RestService} from '../providers/rest.service';
import * as XLSX from 'xlsx';

class Actividad {
  constructor(
    public idACTIVIDAD: string = '',
    public nomACTIVIDAD: string = '',
    public desACTIVIDAD: string = ''
  ) {}
}

@Component({
  selector: 'app-actividad',
  templateUrl: './actividad.component.html',
  styleUrls: ['./actividad.component.css']
})
export class ActividadComponent implements OnInit {
  formActividad: FormGroup;
  @ViewChild('modalClose') modalClose:ElementRef;
  
  // It maintains list of actividades
  actividades: Actividad[] = [];
  // It maintains actividad Model
  regModel: Actividad;
  // It maintains actividad form display status. By default it will be false.
  showNew: Boolean = false;
  // It will be either 'Save' or 'Update' based on operation.
  submitType: string = 'Save';
  // It maintains table row index based on selection.
  selectedRow: number;
  // It maintains Array of countries.
  totalRec : number;
  page: number = 1;

  selectedIndex : number;

  public filtroNombre: String = null;

  constructor(private restService:RestService) {
    
    this.consultarActividades();

  }

  consultarActividades(){
    // Add default actividad data.

    this.actividades = [];

    this.restService.
      consultarActividades()
        .subscribe(data => {
          if (data.success === true) {

            var arrayActividades = [];
            arrayActividades = data.actividades;

            this.totalRec = arrayActividades.length;

            for (let actividad of arrayActividades) {
              console.log("ACTIVIDAD ENCONTRADA",actividad);
              this.actividades.push(new Actividad(actividad.idACTIVIDAD,actividad.nomACTIVIDAD, actividad.desACTIVIDAD));
           }
          
  
          } else {
            console.log("ERROR AL OBTENER ACTIVIDADS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });
  }

  ngOnInit() {
    this.formActividad = new FormGroup({
      'idACTIVIDAD': new FormControl({value:null,disabled: true}),
      'nomACTIVIDAD': new FormControl(null, Validators.compose([Validators.required,Validators.pattern(".*[^ ].*")])), 
      'desACTIVIDAD': new FormControl(null, Validators.compose([Validators.required,Validators.pattern(".*[^ ].*")]))
		});
    
  }

  // This method associate to New Button.
  onNew() {
    // Initiate new actividad.
    this.regModel = new Actividad();
    this.formActividad.reset();
    // Change submitType to 'Save'.
    this.submitType = 'Save';
    // display actividad entry section.
    this.showNew = true;
  }

  filtrar(){
    var date = new Date();

    if (this.filtroNombre!=null && this.filtroNombre!=''){

      var filtros = { 
          nomACTIVIDAD:this.filtroNombre

      }

      this.restService.
       consultarActividadesFiltros(filtros)
        .subscribe(data => {
          if (data.success === true) {

            this.actividades = data.actividades;

            this.totalRec = this.actividades.length;
          
          } else {
            console.log("ERROR AL CONSULTAR ACTIVIDADS FILTROS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });

    }else{
      this.consultarActividades();
    }

    
  
  }

  // This method associate to Save Button.
  onSave() {
    
    if(this.formActividad.valid){

    if (this.submitType === 'Save') {
      // Push actividad model object into actividad list.

      var actividad = {
        nomACTIVIDAD:this.regModel.nomACTIVIDAD.trim(),
        desACTIVIDAD:this.regModel.desACTIVIDAD.trim()
      }
      this.restService.
       insertarActividad(actividad)
        .subscribe(data => {
          if (data.success === true) {

            this.actividades.push(data.actividad);
          
          } else {
            console.log("ERROR AL INSERTAR ACTIVIDADS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });


    } else {
      // Update the existing properties values based on model.

      this.restService.
       editarActividad(this.regModel.idACTIVIDAD,this.regModel)
        .subscribe(data => {
          if (data.success === true) {
          this.actividades[this.selectedRow].idACTIVIDAD = this.regModel.idACTIVIDAD;
          this.actividades[this.selectedRow].nomACTIVIDAD = this.regModel.nomACTIVIDAD;
          this.actividades[this.selectedRow].desACTIVIDAD = this.regModel.desACTIVIDAD;
          } else {
            console.log("ERROR AL EDITAR ACTIVIDADS");
          }
        });
    }
    // Hide actividad entry section.
    this.showNew = false;
    this.modalClose.nativeElement.click();
    }
  }

  // This method associate to Edit Button.
  onEdit(index: number) {
    // Assign selected table row index.
    this.selectedRow = index;
    // Initiate new actividad.
    this.regModel = new Actividad();

    console.log('ACTIVIDAD EDITAR',this.actividades[this.selectedRow])
    // Retrieve selected actividad from list and assign to model.
    this.regModel = Object.assign({}, this.actividades[this.selectedRow]);
    // Change submitType to Update.
    this.submitType = 'Update';
    // Display actividad entry section.
    this.showNew = true;
  }

  // This method associate to Delete Button.
  confirmDelete(index: number) {
    console.log("AQUI CONFIRMAR ELIMINACION",index);
    this.selectedIndex = index;
  }

  // This method associate to Delete Button.
  onDelete(index: number) {
    // Delete the corresponding actividad entry from the list.
    console.log("AQUI CONFIRMAR ELIMINACION",index);
    this.restService.
      eliminarActividad(this.actividades[index].idACTIVIDAD)
        .subscribe(data => {
          if (data.success === true) {
            this.actividades.splice(index,1);
          } else {
            console.log("ERROR AL ELIMINAR ACTIVIDADS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });
    
    
  }

  // This method associate toCancel Button.
  onCancel() {
    // Hide actividad entry section.
    this.showNew = false;
  }

 

  exportarExcel(){

    /* make the worksheet */
    var ws = XLSX.utils.json_to_sheet(this.actividades);

    /* add to workbook */
    var wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "People");

    /* write workbook (use type 'binary') */
    var wbout = XLSX.write(wb, {bookType:'xlsx', type:'binary'});

    var today = new Date();
    var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
    var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
    var name = "reporteActividad" + date + time;

    FileSaver.saveAs(new Blob([this.s2ab(wbout)],{type:"application/octet-stream"}), name+".xlsx");
  }

   s2ab(s) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
  }

   exportAsExcelFile(json: any[], excelFileName: string): void {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }

    private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
          type: "application/octet-stream"
        });
        var today = new Date();
        var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
        var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
        var name = fileName + date + time;
        FileSaver.saveAs(data, name + ".xlsx");
}




}
