
import { Injectable } from '@angular/core';
import { Router, CanActivate ,ActivatedRouteSnapshot} from '@angular/router';

@Injectable()
export class UserService implements CanActivate {

  private isUserLoggedIn;
  public username;
  public codRol;
  public login;

  constructor( private router:Router) { 
  	this.isUserLoggedIn = false;
  }

  setUserLoggedIn() {
  	this.isUserLoggedIn = true;
  }

  getUserLoggedIn() {
  	return this.isUserLoggedIn;
  }


  canActivate(route: ActivatedRouteSnapshot): boolean {

    const expectedRole = route.data.expectedRole;

    if (!this.isUserLoggedIn) {
      this.router.navigate(['']);
      return false;
     
    }else if(expectedRole == "All"){

      return true;
      
    }else if (!this.codRol == expectedRole){

      this.router.navigate(['']);
      return false;
    }



    return true;
  }


  salir(){
    this.isUserLoggedIn = false;
    this.username='';
    this.codRol='';
    this.login='';
    this.router.navigate(['']);
  }

}
