import { Component, OnInit,ViewChild, ElementRef} from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import FileSaver from 'file-saver';
import { RestService} from '../providers/rest.service';
import * as XLSX from 'xlsx';

class Equipo {
  constructor(
    public idEQUIPO: string = '',
    public nomEQUIPO: string = '',
    public desEQUIPO: string = '',
    public precio : string = '0'
  ) {}
}

@Component({
  selector: 'app-equipo',
  templateUrl: './equipo.component.html',
  styleUrls: ['./equipo.component.css']
})
export class EquipoComponent implements OnInit {
  formEquipo: FormGroup;
  @ViewChild('modalClose') modalClose:ElementRef;
  
  // It maintains list of equipos
  equipos: Equipo[] = [];
  // It maintains equipo Model
  regModel: Equipo;
  // It maintains equipo form display status. By default it will be false.
  showNew: Boolean = false;
  // It will be either 'Save' or 'Update' based on operation.
  submitType: string = 'Save';
  // It maintains table row index based on selection.
  selectedRow: number;
  // It maintains Array of countries.
  totalRec : number;
  page: number = 1;

  selectedIndex : number;

  public filtroNombre: String = null;

  constructor(private restService:RestService) {
    
    this.consultarEquipos();

  }

  consultarEquipos(){
    // Add default equipo data.

    this.equipos = [];

    this.restService.
      consultarEquipos()
        .subscribe(data => {
          if (data.success === true) {

            var arrayEquipos = [];
            arrayEquipos = data.equipos;

            this.totalRec = arrayEquipos.length;

            for (let equipo of arrayEquipos) {
              console.log("EQUIPO ENCONTRADA",equipo);
              this.equipos.push(new Equipo(equipo.idEQUIPO,equipo.nomEQUIPO, equipo.desEQUIPO,equipo.precio));
           }
          
  
          } else {
            console.log("ERROR AL OBTENER EQUIPOS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });
  }

  ngOnInit() {
    this.formEquipo = new FormGroup({
      'idEQUIPO': new FormControl({value:null,disabled: true}),
      'nomEQUIPO': new FormControl(null, Validators.compose([Validators.required,Validators.pattern(".*[^ ].*")])), 
      'desEQUIPO': new FormControl(null, Validators.compose([Validators.required,Validators.pattern(".*[^ ].*")])),
      'precio': new FormControl(null, Validators.compose([Validators.required,Validators.pattern(".*[^ ].*")]))
		});
    
  }

  // This method associate to New Button.
  onNew() {
    // Initiate new equipo.
    this.regModel = new Equipo();
    this.formEquipo.reset();
    // Change submitType to 'Save'.
    this.submitType = 'Save';
    // display equipo entry section.
    this.showNew = true;
  }

  filtrar(){
    var date = new Date();

    if (this.filtroNombre!=null && this.filtroNombre!=''){

      var filtros = { 
          nomEQUIPO:this.filtroNombre

      }

      this.restService.
       consultarEquiposFiltros(filtros)
        .subscribe(data => {
          if (data.success === true) {

            this.equipos = data.equipos;

            this.totalRec = this.equipos.length;
          
          } else {
            console.log("ERROR AL CONSULTAR EQUIPOS FILTROS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });

    }else{
      this.consultarEquipos();
    }

    
  
  }

  // This method associate to Save Button.
  onSave() {
    
    if(this.formEquipo.valid){

    if (this.submitType === 'Save') {
      // Push equipo model object into equipo list.

      var equipo = {
        nomEQUIPO:this.regModel.nomEQUIPO.trim(),
        desEQUIPO:this.regModel.desEQUIPO.trim()
      }
      this.restService.
       insertarEquipo(equipo)
        .subscribe(data => {
          if (data.success === true) {

            this.equipos.push(data.equipo);
          
          } else {
            console.log("ERROR AL INSERTAR EQUIPOS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });


    } else {
      // Update the existing properties values based on model.

      this.restService.
       editarEquipo(this.regModel.idEQUIPO,this.regModel)
        .subscribe(data => {
          if (data.success === true) {
          this.equipos[this.selectedRow].idEQUIPO = this.regModel.idEQUIPO;
          this.equipos[this.selectedRow].nomEQUIPO = this.regModel.nomEQUIPO;
          this.equipos[this.selectedRow].desEQUIPO = this.regModel.desEQUIPO;
          this.equipos[this.selectedRow].precio = this.regModel.precio;
          } else {
            console.log("ERROR AL EDITAR EQUIPOS");
          }
        });
    }
    // Hide equipo entry section.
    this.showNew = false;
    this.modalClose.nativeElement.click();
    }
  }

  // This method associate to Edit Button.
  onEdit(index: number) {
    // Assign selected table row index.
    this.selectedRow = index;
    // Initiate new equipo.
    this.regModel = new Equipo();

    console.log('EQUIPO EDITAR',this.equipos[this.selectedRow])
    // Retrieve selected equipo from list and assign to model.
    this.regModel = Object.assign({}, this.equipos[this.selectedRow]);
    // Change submitType to Update.
    this.submitType = 'Update';
    // Display equipo entry section.
    this.showNew = true;
  }

  // This method associate to Delete Button.
  confirmDelete(index: number) {
    console.log("AQUI CONFIRMAR ELIMINACION",index);
    this.selectedIndex = index;
  }

  // This method associate to Delete Button.
  onDelete(index: number) {
    // Delete the corresponding equipo entry from the list.
    console.log("AQUI CONFIRMAR ELIMINACION",index);
    this.restService.
      eliminarEquipo(this.equipos[index].idEQUIPO)
        .subscribe(data => {
          if (data.success === true) {
            this.equipos.splice(index,1);
          } else {
            console.log("ERROR AL ELIMINAR EQUIPOS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });
    
    
  }

  // This method associate toCancel Button.
  onCancel() {
    // Hide equipo entry section.
    this.showNew = false;
  }

 

  exportarExcel(){

    /* make the worksheet */
    var ws = XLSX.utils.json_to_sheet(this.equipos);

    /* add to workbook */
    var wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "People");

    /* write workbook (use type 'binary') */
    var wbout = XLSX.write(wb, {bookType:'xlsx', type:'binary'});

    var today = new Date();
    var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
    var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
    var name = "reporteEquipo" + date + time;

    FileSaver.saveAs(new Blob([this.s2ab(wbout)],{type:"application/octet-stream"}), name+".xlsx");
  }

   s2ab(s) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
  }

   exportAsExcelFile(json: any[], excelFileName: string): void {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }

    private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
          type: "application/octet-stream"
        });
        var today = new Date();
        var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
        var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
        var name = fileName + date + time;
        FileSaver.saveAs(data, name + ".xlsx");
}




}
