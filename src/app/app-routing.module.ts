import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { RegistrationComponent } from './registration/registration.component';
import { ObraComponent } from './obra/obra.component';
import { ClienteComponent } from './cliente/cliente.component';
import { EquipoComponent } from './equipo/equipo.component';
import { MaterialComponent } from './material/material.component';
import { ProveedorComponent } from './proveedor/proveedor.component';
import { PropietarioComponent } from './propietario/propietario.component';
import { LoginformComponent } from './loginform/loginform.component';
import { ControlEquiposComponent } from './control-equipos/control-equipos.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { ActividadComponent } from './actividad/actividad.component';
import { ProveedortComponent } from './proveedort/proveedort.component';
import { ObraProveedortComponent } from './obraProveedort/obraProveedort.component';
import { UserService } from './user.service';

const routes: Routes = [
  {
    path: '',
    component: LoginformComponent
  },
  {
    path: 'registration',
    component: RegistrationComponent,
    canActivate:[UserService],
    data: { 
      expectedRole: 'All'
    } 
  },
  {
    path: 'loginform',
    component: LoginformComponent
  }
  ,
  {
    path: 'control-equipos',
    component: ControlEquiposComponent,
    canActivate:[UserService],
    data: { 
      expectedRole: 'All'
    } 
  },
  {
    path: 'obra',
    component: ObraComponent,
    canActivate:[UserService],
    data: { 
      expectedRole: '1'
    } 
  },
  {
    path: 'cliente',
    component: ClienteComponent,
    canActivate:[UserService],
    data: { 
      expectedRole: '1'
    } 
  }
  ,
  {
    path: 'actividad',
    component: ActividadComponent,
    canActivate:[UserService],
    data: { 
      expectedRole: '1'
    } 
  }
  ,
  {
    path: 'equipo',
    component: EquipoComponent,
    canActivate:[UserService],
    data: { 
      expectedRole: '1'
    } 
  },
  {
    path: 'material',
    component: MaterialComponent,
    canActivate:[UserService],
    data: { 
      expectedRole: '1'
    } 
  },
  {
    path: 'proveedor',
    component: ProveedorComponent,
    canActivate:[UserService],
    data: { 
      expectedRole: '1'
    } 
  },
  {
    path: 'propietario',
    component: PropietarioComponent,
    canActivate:[UserService],
    data: { 
      expectedRole: '1'
    } 
  }
  ,
  {
    path: 'usuario',
    component: UsuarioComponent,
    canActivate:[UserService],
    data: { 
      expectedRole: '1'
    } 
  },
  {
    path: 'proveedort',
    component: ProveedortComponent,
    canActivate:[UserService],
    data: { 
      expectedRole: '1'
    } 
  },
  {
    path: 'obraProveedort',
    component: ObraProveedortComponent,
    canActivate:[UserService],
    data: { 
      expectedRole: '1'
    } 
  }

  
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
