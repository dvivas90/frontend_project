import { Component, OnInit,ViewChild, ElementRef} from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import FileSaver from 'file-saver';
import { RestService} from '../providers/rest.service';
import * as XLSX from 'xlsx';

class Proveedort {
  constructor(
    public idPROVEEDORT: string = '',
    public nomPROVEEDORT: string = '',
    public desPROVEEDORT: string = ''
  ) {}
}

@Component({
  selector: 'app-proveedort',
  templateUrl: './proveedort.component.html',
  styleUrls: ['./proveedort.component.css']
})
export class ProveedortComponent implements OnInit {
  formProveedort: FormGroup;
  @ViewChild('modalClose') modalClose:ElementRef;
  
  // It maintains list of proveedorest
  proveedorest: Proveedort[] = [];
  // It maintains proveedort Model
  regModel: Proveedort;
  // It maintains proveedort form display status. By default it will be false.
  showNew: Boolean = false;
  // It will be either 'Save' or 'Update' based on operation.
  submitType: string = 'Save';
  // It maintains table row index based on selection.
  selectedRow: number;
  // It maintains Array of countries.
  totalRec : number;
  page: number = 1;

  selectedIndex : number;

  public filtroNombre: String = null;

  constructor(private restService:RestService) {
    
    this.consultarProveedorest();

  }

  consultarProveedorest(){
    // Add default proveedort data.

    this.proveedorest = [];

    this.restService.
      consultarProveedorest()
        .subscribe(data => {
          if (data.success === true) {

            var arrayProveedorest = [];
            arrayProveedorest = data.proveedorest;

            this.totalRec = arrayProveedorest.length;

            for (let proveedort of arrayProveedorest) {
              console.log("PROVEEDORT ENCONTRADA",proveedort);
              this.proveedorest.push(new Proveedort(proveedort.idPROVEEDORT,proveedort.nomPROVEEDORT, proveedort.desPROVEEDORT));
           }
          
  
          } else {
            console.log("ERROR AL OBTENER PROVEEDORTS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });
  }

  ngOnInit() {
    this.formProveedort = new FormGroup({
      'idPROVEEDORT': new FormControl({value:null,disabled: true}),
      'nomPROVEEDORT': new FormControl(null, Validators.compose([Validators.required,Validators.pattern(".*[^ ].*")])), 
      'desPROVEEDORT': new FormControl(null, Validators.compose([Validators.required,Validators.pattern(".*[^ ].*")]))
		});
    
  }

  // This method associate to New Button.
  onNew() {
    // Initiate new proveedort.
    this.regModel = new Proveedort();
    this.formProveedort.reset();
    // Change submitType to 'Save'.
    this.submitType = 'Save';
    // display proveedort entry section.
    this.showNew = true;
  }

  filtrar(){
    var date = new Date();

    if (this.filtroNombre!=null && this.filtroNombre!=''){

      var filtros = { 
          nomPROVEEDORT:this.filtroNombre

      }

      this.restService.
       consultarProveedorestFiltros(filtros)
        .subscribe(data => {
          if (data.success === true) {

            this.proveedorest = data.proveedorest;

            this.totalRec = this.proveedorest.length;
          
          } else {
            console.log("ERROR AL CONSULTAR PROVEEDORTS FILTROS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });

    }else{
      this.consultarProveedorest();
    }

    
  
  }

  // This method associate to Save Button.
  onSave() {
    
    if(this.formProveedort.valid){

    if (this.submitType === 'Save') {
      // Push proveedort model object into proveedort list.

      var proveedort = {
        nomPROVEEDORT:this.regModel.nomPROVEEDORT.trim(),
        desPROVEEDORT:this.regModel.desPROVEEDORT.trim()
      }
      this.restService.
       insertarProveedort(proveedort)
        .subscribe(data => {
          if (data.success === true) {

            this.proveedorest.push(data.proveedort);
          
          } else {
            console.log("ERROR AL INSERTAR PROVEEDORTS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });


    } else {
      // Update the existing properties values based on model.

      this.restService.
       editarProveedort(this.regModel.idPROVEEDORT,this.regModel)
        .subscribe(data => {
          if (data.success === true) {
          this.proveedorest[this.selectedRow].idPROVEEDORT = this.regModel.idPROVEEDORT;
          this.proveedorest[this.selectedRow].nomPROVEEDORT = this.regModel.nomPROVEEDORT;
          this.proveedorest[this.selectedRow].desPROVEEDORT = this.regModel.desPROVEEDORT;
          } else {
            console.log("ERROR AL EDITAR PROVEEDORTS");
          }
        });
    }
    // Hide proveedort entry section.
    this.showNew = false;
    this.modalClose.nativeElement.click();
    }
  }

  // This method associate to Edit Button.
  onEdit(index: number) {
    // Assign selected table row index.
    this.selectedRow = index;
    // Initiate new proveedort.
    this.regModel = new Proveedort();

    console.log('PROVEEDORT EDITAR',this.proveedorest[this.selectedRow])
    // Retrieve selected proveedort from list and assign to model.
    this.regModel = Object.assign({}, this.proveedorest[this.selectedRow]);
    // Change submitType to Update.
    this.submitType = 'Update';
    // Display proveedort entry section.
    this.showNew = true;
  }

  // This method associate to Delete Button.
  confirmDelete(index: number) {
    console.log("AQUI CONFIRMAR ELIMINACION",index);
    this.selectedIndex = index;
  }

  // This method associate to Delete Button.
  onDelete(index: number) {
    // Delete the corresponding proveedort entry from the list.
    console.log("AQUI CONFIRMAR ELIMINACION",index);
    this.restService.
      eliminarProveedort(this.proveedorest[index].idPROVEEDORT)
        .subscribe(data => {
          if (data.success === true) {
            this.proveedorest.splice(index,1);
          } else {
            console.log("ERROR AL ELIMINAR PROVEEDORTS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });
    
    
  }

  // This method associate toCancel Button.
  onCancel() {
    // Hide proveedort entry section.
    this.showNew = false;
  }

 

  exportarExcel(){

    /* make the worksheet */
    var ws = XLSX.utils.json_to_sheet(this.proveedorest);

    /* add to workbook */
    var wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "People");

    /* write workbook (use type 'binary') */
    var wbout = XLSX.write(wb, {bookType:'xlsx', type:'binary'});

    var today = new Date();
    var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
    var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
    var name = "reporteProveedort" + date + time;

    FileSaver.saveAs(new Blob([this.s2ab(wbout)],{type:"application/octet-stream"}), name+".xlsx");
  }

   s2ab(s) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
  }

   exportAsExcelFile(json: any[], excelFileName: string): void {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }

    private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
          type: "application/octet-stream"
        });
        var today = new Date();
        var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
        var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
        var name = fileName + date + time;
        FileSaver.saveAs(data, name + ".xlsx");
}




}
