import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import FileSaver from 'file-saver';
import { RestService } from '../providers/rest.service';
import * as XLSX from 'xlsx';

class ObraProveedort {
  constructor(
    public idOBRAPROVEEDORT: string = '',
    public obra: string[] = [null, null],
    public proveedort: string[] = [null, null],
    public precio: string = ''
  ) { }
}

@Component({
  selector: 'app-ObraProveedort',
  templateUrl: './obraProveedort.component.html',
  styleUrls: ['./obraProveedort.component.css']
})
export class ObraProveedortComponent implements OnInit {
  formObraProveedort: FormGroup;
  @ViewChild('modalClose') modalClose: ElementRef;

  // It maintains list of obraProveedorest
  obraProveedorest: ObraProveedort[] = [];
  // It maintains obraObraProveedort Model
  regModel: ObraProveedort;

  proveedorest: string[];

  obras: string[];


  proveedortValido: boolean = true;

  obraValido: boolean = true;

  precioValido: boolean = true;


  // It maintains obraObraProveedort form display status. By default it will be false.
  showNew: Boolean = false;
  // It will be either 'Save' or 'Update' based on operation.
  submitType: string = 'Save';
  // It maintains table row index based on selection.
  selectedRow: number;
  // It maintains Array of countries.
  totalRec: number;
  page: number = 1;

  selectedIndex: number;

  public filtroNombreObra: String = null;

  public filtroNombreProveedort: String = null;

  constructor(private restService: RestService) {

    this.consultarObraProveedorest();
    this.consultarObras();
    this.consultarProveedorest();

  }

  consultarObraProveedorest() {
    // Add default obraObraProveedort data.

    this.obraProveedorest = [];

    this.restService.
      consultarObraProveedorest()
      .subscribe(data => {
        if (data.success === true) {

          var arrayObraProveedorest = [];
          arrayObraProveedorest = data.obraProveedorest;

          this.totalRec = arrayObraProveedorest.length;

          for (let obraObraProveedort of arrayObraProveedorest) {
            console.log("OBRAPROVEEDORT ENCONTRADA", obraObraProveedort);

            var proveedort = [obraObraProveedort.proveedort.idPROVEEDORT, obraObraProveedort.proveedort.nomPROVEEDORT]
            var obra = [obraObraProveedort.obra.idOBRA, obraObraProveedort.obra.nomOBRA]
            this.obraProveedorest.push(new ObraProveedort(obraObraProveedort.idOBRAPROVEEDORT, obra ,proveedort , obraObraProveedort.precio));
          }


        } else {
          console.log("ERROR AL OBTENER OBRAPROVEEDORTS");
          //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
        }
      });
  }

  consultarObraProveedorestFiltros(filtro) {
    // Add default obraObraProveedort data.

    this.obraProveedorest = [];

    this.restService.
      consultarObraProveedorestFiltros(filtro)
      .subscribe(data => {
        if (data.success === true) {

          var arrayObraProveedorest = [];
          arrayObraProveedorest = data.obraProveedorest;

          this.totalRec = arrayObraProveedorest.length;

          for (let obraObraProveedort of arrayObraProveedorest) {
            console.log("PROVEEDORT ENCONTRADA", obraObraProveedort);

            var proveedort = [obraObraProveedort.proveedort.idPROVEEDORT, obraObraProveedort.proveedort.nomPROVEEDORT]
            var obra = [obraObraProveedort.obra.idOBRA, obraObraProveedort.obra.nomOBRA]
            this.obraProveedorest.push(new ObraProveedort(obraObraProveedort.idOBRAPROVEEDORT,obra, proveedort, obraObraProveedort.precio));
          }


        } else {
          console.log("ERROR AL OBTENER PROVEEDORTS");
          //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
        }
      });
  }

  ngOnInit() {
    this.formObraProveedort = new FormGroup({
      'idOBRAPROVEEDORT': new FormControl({ value: null, disabled: true }),
      'obra': new FormControl(null, Validators.compose([Validators.required, Validators.pattern(".*[^ ].*")])),
      'proveedort': new FormControl(null, Validators.compose([Validators.required, Validators.pattern(".*[^ ].*")])),
      'precio': new FormControl(null, Validators.compose([Validators.required, Validators.pattern(".*[^ ].*")]))
    });

  }

  // This method associate to New Button.
  onNew() {
    // Initiate new obraObraProveedort.
    this.regModel = new ObraProveedort();
    this.formObraProveedort.reset();
    // Change submitType to 'Save'.
    this.submitType = 'Save';
    // display obraObraProveedort entry section.
    this.showNew = true;
  }


  consultarProveedorest() {

    this.restService.
      consultarProveedorest()
      .subscribe(data => {
        if (data.success === true) {

          this.proveedorest = data.proveedorest;

        } else {
          console.log("ERROR AL OBTENER PROVEEDOREST");
          //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
        }
      });

  }

  consultarObras() {

    this.restService.
      consultarObras()
      .subscribe(data => {
        if (data.success === true) {

          this.obras = data.obras;

        } else {
          console.log("ERROR AL OBTENER OBRAS");
          //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
        }
      });

  }

  validarCampos() {

    if (!this.formObraProveedort.controls['proveedort'].valid) {
      this.proveedortValido = false;
    };

    if (!this.formObraProveedort.controls['obra'].valid) {
      this.obraValido = false;
    };

    if (!this.formObraProveedort.controls['precio'].valid) {
      this.precioValido = false;
    };

  }


  limpiarValidaciones() {

    this.precioValido = true;
    this.obraValido = true;
    this.proveedortValido = true;
  }

  filtrar() {

    var filtros = {}
    var filtrar = false;

    if (this.filtroNombreObra != null && this.filtroNombreObra != '') {

      filtros['nomOBRA'] = this.filtroNombreObra;
      filtrar = true;

    }

    if (this.filtroNombreProveedort != null && this.filtroNombreProveedort != '') {

      filtros['nomPROVEEDORT'] = this.filtroNombreProveedort;
      filtrar = true;

    }

    if (filtrar==true){
      this.consultarObraProveedorestFiltros(filtros);
    }else{
      this.consultarObraProveedorest();
    }

    
  
  }

// This method associate to Save Button.
onSave() {

  this.validarCampos();

  if (this.formObraProveedort.valid) {

    var obraProveedort = {
      idOBRA: this.regModel.obra[0],
      idPROVEEDORT: this.regModel.proveedort[0],
      precio: this.regModel.precio
    }

    if (this.submitType === 'Save') {
      // Push obraObraProveedort model object into obraObraProveedort list.


      this.restService.
        insertarObraProveedort(obraProveedort)
        .subscribe(data => {
          if (data.success === true) {

            this.consultarObraProveedorest();

          } else {
            console.log("ERROR AL INSERTAR OBRAPROVEEDORTS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });


    } else {
      // Update the existing properties values based on model.

      this.restService.
        editarObraProveedort(this.regModel.idOBRAPROVEEDORT, obraProveedort)
        .subscribe(data => {
          if (data.success === true) {
            this.consultarObraProveedorest();
          } else {
            console.log("ERROR AL EDITAR OBRAPROVEEDORTS");
          }
        });
    }
    // Hide obraObraProveedort entry section.
    this.showNew = false;
    this.modalClose.nativeElement.click();
  }
}

// This method associate to Edit Button.
onEdit(index: number) {
  // Assign selected table row index.
  this.selectedRow = index;
  // Initiate new obraObraProveedort.
  this.regModel = new ObraProveedort();

  console.log('OBRAPROVEEDORT EDITAR', this.obraProveedorest[this.selectedRow])
  // Retrieve selected obraObraProveedort from list and assign to model.
  this.regModel = Object.assign({}, this.obraProveedorest[this.selectedRow]);
  // Change submitType to Update.
  this.submitType = 'Update';
  // Display obraObraProveedort entry section.
  this.showNew = true;
}

onChangeProveedort(proveedort) {
  // Assign corresponding selected country to model.
  this.regModel.proveedort[0] = proveedort.idPROVEEDORT;
  this.regModel.proveedort[1] = proveedort.nomPROVEEDORT;
}

// This method associate to Bootstrap dropdown selection change.
onChangeObra(obra) {
  // Assign corresponding selected country to model.
  this.regModel.obra[0] = obra.idOBRA;
  this.regModel.obra[1] = obra.nomOBRA;
}

// This method associate to Delete Button.
confirmDelete(index: number) {
  console.log("AQUI CONFIRMAR ELIMINACION", index);
  this.selectedIndex = index;
}

// This method associate to Delete Button.
onDelete(index: number) {
  // Delete the corresponding obraObraProveedort entry from the list.
  console.log("AQUI CONFIRMAR ELIMINACION", index);
  this.restService.
    eliminarObraProveedort(this.obraProveedorest[index].idOBRAPROVEEDORT)
    .subscribe(data => {
      if (data.success === true) {
        this.obraProveedorest.splice(index, 1);
      } else {
        console.log("ERROR AL ELIMINAR PROVEEDORTS");
        //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
      }
    });


}

// This method associate toCancel Button.
onCancel() {
  // Hide obraObraProveedort entry section.
  this.showNew = false;
}



exportarExcel(){

  /* make the worksheet */
  var ws = XLSX.utils.json_to_sheet(this.obraProveedorest);

  /* add to workbook */
  var wb = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, "People");

  /* write workbook (use type 'binary') */
  var wbout = XLSX.write(wb, { bookType: 'xlsx', type: 'binary' });

  var today = new Date();
  var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
  var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
  var name = "reporteObraProveedort" + date + time;

  FileSaver.saveAs(new Blob([this.s2ab(wbout)], { type: "application/octet-stream" }), name + ".xlsx");
}

s2ab(s) {
  var buf = new ArrayBuffer(s.length);
  var view = new Uint8Array(buf);
  for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
  return buf;
}

exportAsExcelFile(json: any[], excelFileName: string): void {
  const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
  const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
  const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
  this.saveAsExcelFile(excelBuffer, excelFileName);
}

    private saveAsExcelFile(buffer: any, fileName: string): void {
  const data: Blob = new Blob([buffer], {
    type: "application/octet-stream"
  });
  var today = new Date();
  var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
  var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
  var name = fileName + date + time;
  FileSaver.saveAs(data, name + ".xlsx");
}




}
