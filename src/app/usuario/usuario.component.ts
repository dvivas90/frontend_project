import { Component, OnInit,ViewChild, ElementRef} from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import FileSaver from 'file-saver';
import { RestService} from '../providers/rest.service';
import * as XLSX from 'xlsx';

class Usuario {
  constructor(
    public idUSUARIO: string = '',
    public login: string = '',
    public nombre: string = '',
    public password: string = '',
    public rol: string[] = [null,null]
  ) {}
}

@Component({
  selector: 'app-Usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {
  formUsuario: FormGroup;
  @ViewChild('modalClose') modalClose:ElementRef;
  
  // It maintains list of usuarios
  usuarios: Usuario[] = [];
  // It maintains Usuario Model
  regModel: Usuario;
  // It maintains Usuario form display status. By default it will be false.
  showNew: Boolean = false;
  // It will be either 'Save' or 'Update' based on operation.
  submitType: string = 'Save';
  // It maintains table row index based on selection.
  selectedRow: number;
  // It maintains Array of countries.
  totalRec : number;
  page: number = 1;

  selectedIndex : number;

  public filtroNombre: String = null;

  roles: string[] ;

  showPassword : boolean = false;


  constructor(private restService:RestService) {
    this.consultarRoles();
    this.consultarUsuarios();

  }

  verPassword(){
    this.showPassword = !this.showPassword;

  }

  consultarRoles(){

    this.restService.
    consultarRoles()
      .subscribe(data => {
        if (data.success === true) {

          this.roles = data.roles;

        } else {
          console.log("ERROR AL OBTENER ROLES");
          //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
        }
      });

  }

  consultarUsuarios(){
    // Add default Usuario data.

    this.usuarios = [];

    this.restService.
      consultarUsuarios()
        .subscribe(data => {
          if (data.success === true) {

            var arrayUsuarios = [];
            arrayUsuarios = data.usuarios;

            this.totalRec = arrayUsuarios.length;

            for (let usuario of arrayUsuarios) {
              var rol = [usuario.rol.idROL,usuario.rol.desROL ]
              this.usuarios.push(new Usuario(usuario.idUSUARIO, usuario.login, usuario.nombre, usuario.password,  rol));
           }
          
  
          } else {
            console.log("ERROR AL OBTENER USUARIOS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });
  }

  ngOnInit() {
    this.formUsuario = new FormGroup({
      'idUSUARIO': new FormControl({value:null,disabled: true}),
      'nombre': new FormControl(null, Validators.compose([Validators.required,Validators.pattern(".*[^ ].*")])), 
      'login': new FormControl(null, Validators.compose([Validators.required,Validators.pattern(".*[^ ].*")])),
      'password': new FormControl(null, Validators.compose([Validators.required,Validators.pattern(".*[^ ].*")])),
      'rol': new FormControl('null', Validators.compose([Validators.required, Validators.pattern('.*[^ ].*')]))
		});
    
  }

  // This method associate to New Button.
  onNew() {
    // Initiate new Usuario.
    this.regModel = new Usuario();
    this.formUsuario.reset();
    // Change submitType to 'Save'.
    this.submitType = 'Save';
    // display Usuario entry section.
    this.showNew = true;
  }

  filtrar(){

    if (this.filtroNombre!=null && this.filtroNombre!=''){

      var filtros = { 
          nombre:this.filtroNombre

      }

      this.restService.
       consultarUsuarioFiltros(filtros)
        .subscribe(data => {
          if (data.success === true) {

            this.usuarios  = [];
            var arrayUsuarios = [];
            arrayUsuarios = data.usuarios;

            this.totalRec = arrayUsuarios.length;

            for (let usuario of arrayUsuarios) {
              var rol = [usuario.rol.idROL,usuario.rol.desROL ]
              this.usuarios.push(new Usuario(usuario.idUSUARIO, usuario.login, usuario.nombre,usuario.password,  rol));
           }
          
          } else {
            console.log("ERROR AL CONSULTAR USUARIOS FILTROS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });

    }else{
      this.consultarUsuarios();
    }

    
  
  }

  // This method associate to Save Button.
  onSave() {
    
    if(this.formUsuario.valid){

    if (this.submitType === 'Save') {
      // Push Usuario model object into Usuario list.

      var usuario = {
        nombre:this.regModel.nombre.trim(),
        login:this.regModel.login.trim(),
        password:this.regModel.password.trim(),
        idROL:  this.regModel.rol[0]
      }
      this.restService.
       insertarUsuario(usuario)
        .subscribe(data => {
          if (data.success === true) {

            this.consultarUsuarios();
          
          } else {
            console.log("ERROR AL INSERTAR USUARIOS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });


    } else {
      // Update the existing properties values based on model.


      var datos = {
        idUSUARIO :  this.regModel.idUSUARIO,
        idROL:  this.regModel.rol[0],
        login : this.regModel.login,
        password : this.regModel.password,
        nombre : this.regModel.nombre
        
      }

      this.restService.
       editarUsuario(this.regModel.idUSUARIO,datos)
        .subscribe(data => {
          if (data.success === true) {
          this.usuarios[this.selectedRow].idUSUARIO = this.regModel.idUSUARIO;
          this.usuarios[this.selectedRow].nombre = this.regModel.nombre;
          this.usuarios[this.selectedRow].login = this.regModel.login;
          this.usuarios[this.selectedRow].password = this.regModel.password;
          this.usuarios[this.selectedRow].rol = this.regModel.rol;
          } else {
            console.log("ERROR AL EDITAR USUARIOS");
          }
        });
    }
    // Hide Usuario entry section.
    this.showNew = false;
    this.modalClose.nativeElement.click();
    }
  }

  // This method associate to Edit Button.
  onEdit(index: number) {
    // Assign selected table row index.
    this.selectedRow = index;
    // Initiate new Usuario.
    this.regModel = new Usuario();

    console.log('Usuario EDITAR',this.usuarios[this.selectedRow])
    // Retrieve selected Usuario from list and assign to model.
    this.regModel = Object.assign({}, this.usuarios[this.selectedRow]);
    // Change submitType to Update.
    this.submitType = 'Update';
    // Display Usuario entry section.
    this.showNew = true;
  }

  // This method associate to Delete Button.
  confirmDelete(index: number) {
    console.log("AQUI CONFIRMAR ELIMINACION",index);
    this.selectedIndex = index;
  }

  // This method associate to Delete Button.
  onDelete(index: number) {
    // Delete the corresponding Usuario entry from the list.
    console.log("AQUI CONFIRMAR ELIMINACION",index);
    this.restService.
      eliminarUsuario(this.usuarios[index].idUSUARIO)
        .subscribe(data => {
          if (data.success === true) {
            this.usuarios.splice(index,1);
          } else {
            console.log("ERROR AL ELIMINAR USUARIOS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });
    
    
  }

  // This method associate toCancel Button.
  onCancel() {
    // Hide Usuario entry section.
    this.showNew = false;
  }

  onChangeRol(propietario) {
    // Assign corresponding selected country to model.
    this.regModel.rol[0] = propietario.idROL;
    this.regModel.rol[1] = propietario.desROL;
  }

 

  exportarExcel(){

    /* make the worksheet */
    var ws = XLSX.utils.json_to_sheet(this.usuarios);

    /* add to workbook */
    var wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "People");

    /* write workbook (use type 'binary') */
    var wbout = XLSX.write(wb, {bookType:'xlsx', type:'binary'});

    var today = new Date();
    var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
    var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
    var name = "reporteUsuario" + date + time;

    FileSaver.saveAs(new Blob([this.s2ab(wbout)],{type:"application/octet-stream"}), name+".xlsx");
  }

   s2ab(s) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
  }

   exportAsExcelFile(json: any[], excelFileName: string): void {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }

    private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
          type: "application/octet-stream"
        });
        var today = new Date();
        var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
        var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
        var name = fileName + date + time;
        FileSaver.saveAs(data, name + ".xlsx");
}




}
