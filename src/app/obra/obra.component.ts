import { Component, OnInit,ViewChild, ElementRef} from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import FileSaver from 'file-saver';
import { RestService} from '../providers/rest.service';
import * as XLSX from 'xlsx';

class Obra {
  constructor(
    public idOBRA: string = '',
    public nomOBRA: string = '',
    public desOBRA: string = ''
  ) {}
}

@Component({
  selector: 'app-obra',
  templateUrl: './obra.component.html',
  styleUrls: ['./obra.component.css']
})
export class ObraComponent implements OnInit {
  formObra: FormGroup;
  @ViewChild('modalClose') modalClose:ElementRef;
  
  // It maintains list of obras
  obras: Obra[] = [];
  // It maintains obra Model
  regModel: Obra;
  // It maintains obra form display status. By default it will be false.
  showNew: Boolean = false;
  // It will be either 'Save' or 'Update' based on operation.
  submitType: string = 'Save';
  // It maintains table row index based on selection.
  selectedRow: number;
  // It maintains Array of countries.
  totalRec : number;
  page: number = 1;

  selectedIndex : number;

  public filtroNombre: String = null;

  constructor(private restService:RestService) {
    
    this.consultarObras();

  }

  consultarObras(){
    // Add default obra data.
    this.obras = [];

    this.restService.
      consultarObras()
        .subscribe(data => {
          if (data.success === true) {

            var arrayObras = [];
            arrayObras = data.obras;

            this.totalRec = arrayObras.length;

            for (let obra of arrayObras) {
              console.log("OBRA ENCONTRADA",obra);
              this.obras.push(new Obra(obra.idOBRA,obra.nomOBRA, obra.desOBRA));
           }
          
  
          } else {
            console.log("ERROR AL OBTENER OBRAS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });
  }

  ngOnInit() {
    this.formObra = new FormGroup({
      'idOBRA': new FormControl({value:null,disabled: true}),
      'nomOBRA': new FormControl(null, Validators.compose([Validators.required,Validators.pattern(".*[^ ].*")])), 
      'desOBRA': new FormControl(null, Validators.compose([Validators.required,Validators.pattern(".*[^ ].*")]))
		});
    
  }

  // This method associate to New Button.
  onNew() {
    // Initiate new obra.
    this.regModel = new Obra();
    this.formObra.reset();
    // Change submitType to 'Save'.
    this.submitType = 'Save';
    // display obra entry section.
    this.showNew = true;
  }

  filtrar(){
    var date = new Date();

    if (this.filtroNombre!=null && this.filtroNombre!=''){

      var filtros = { 
          nomOBRA:this.filtroNombre

      }

      this.restService.
       consultarObrasFiltros(filtros)
        .subscribe(data => {
          if (data.success === true) {

            this.obras = data.obras;

            this.totalRec = this.obras.length;
          
          } else {
            console.log("ERROR AL CONSULTAR OBRAS FILTROS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });

    }else{
      this.consultarObras();
    }

    
  
  }

  // This method associate to Save Button.
  onSave() {
    
    if(this.formObra.valid){

    if (this.submitType === 'Save') {
      // Push obra model object into obra list.

      var obra = {
        nomOBRA:this.regModel.nomOBRA.trim(),
        desOBRA:this.regModel.desOBRA.trim()
      }
      this.restService.
       insertarObra(obra)
        .subscribe(data => {
          if (data.success === true) {

            this.obras.push(data.obra);
          
          } else {
            console.log("ERROR AL INSERTAR OBRAS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });


    } else {
      // Update the existing properties values based on model.

      this.restService.
       editarObra(this.regModel.idOBRA,this.regModel)
        .subscribe(data => {
          if (data.success === true) {
          this.obras[this.selectedRow].idOBRA = this.regModel.idOBRA;
          this.obras[this.selectedRow].nomOBRA = this.regModel.nomOBRA;
          this.obras[this.selectedRow].desOBRA = this.regModel.desOBRA;
          } else {
            console.log("ERROR AL EDITAR OBRAS");
          }
        });
    }
    // Hide obra entry section.
    this.showNew = false;
    this.modalClose.nativeElement.click();
    }
  }

  // This method associate to Edit Button.
  onEdit(index: number) {
    // Assign selected table row index.
    this.selectedRow = index;
    // Initiate new obra.
    this.regModel = new Obra();

    console.log('OBRA EDITAR',this.obras[this.selectedRow])
    // Retrieve selected obra from list and assign to model.
    this.regModel = Object.assign({}, this.obras[this.selectedRow]);
    // Change submitType to Update.
    this.submitType = 'Update';
    // Display obra entry section.
    this.showNew = true;
  }

  // This method associate to Delete Button.
  confirmDelete(index: number) {
    console.log("AQUI CONFIRMAR ELIMINACION",index);
    this.selectedIndex = index;
  }

  // This method associate to Delete Button.
  onDelete(index: number) {
    // Delete the corresponding obra entry from the list.
    console.log("AQUI CONFIRMAR ELIMINACION",index);
    this.restService.
      eliminarObra(this.obras[index].idOBRA)
        .subscribe(data => {
          if (data.success === true) {
            this.obras.splice(index,1);
          } else {
            console.log("ERROR AL ELIMINAR OBRAS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });
    
    
  }

  // This method associate toCancel Button.
  onCancel() {
    // Hide obra entry section.
    this.showNew = false;
  }

 

  exportarExcel(){

    /* make the worksheet */
    var ws = XLSX.utils.json_to_sheet(this.obras);

    /* add to workbook */
    var wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "People");

    /* write workbook (use type 'binary') */
    var wbout = XLSX.write(wb, {bookType:'xlsx', type:'binary'});

    var today = new Date();
    var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
    var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
    var name = "reporteObra" + date + time;

    FileSaver.saveAs(new Blob([this.s2ab(wbout)],{type:"application/octet-stream"}), name+".xlsx");
  }

   s2ab(s) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
  }

   exportAsExcelFile(json: any[], excelFileName: string): void {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }

    private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
          type: "application/octet-stream"
        });
        var today = new Date();
        var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
        var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
        var name = fileName + date + time;
        FileSaver.saveAs(data, name + ".xlsx");
}




}
