import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  isCollapsed: Boolean = true;
  constructor(public user:UserService) { }
  ngOnInit() {
    this.user.username='';
    this.user.codRol='';
  
  }


  collapse(){

    this.isCollapsed?this.isCollapsed=true:this.isCollapsed=false;

  }

}
