import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { RestService } from '../providers/rest.service';

@Component({
  selector: 'app-loginform',
  templateUrl: './loginform.component.html',
  styleUrls: ['./loginform.component.css']
})
export class LoginformComponent implements OnInit {
  
  passwordInvalido : boolean = false;

  constructor(private router: Router, private user: UserService, private restService: RestService) { }

  ngOnInit() {
  }

  loginUser(e) {
    e.preventDefault();
    console.log(e);
    var username = e.target.elements[0].value;
    var password = e.target.elements[1].value;

    var filtros = { login : username }


    console.log("FILTROS ",filtros);

    this.restService.
      consultarUsuarioFiltros(filtros)
      .subscribe(data => {
        if (data.success === true) {

          var arrayUsuarios = [];
          arrayUsuarios = data.usuarios;

          if (arrayUsuarios.length>0){

            if (arrayUsuarios[0].password == password){
           

              this.user.setUserLoggedIn();
    
              this.user.username = arrayUsuarios[0].nombre;

              this.user.codRol = arrayUsuarios[0].rol.codROL;

              this.user.login = arrayUsuarios[0].login;

              this.passwordInvalido = false;
              
              this.router.navigate(['registration']);


            }else{

              this.passwordInvalido = true;
            }


          }else{
            this.passwordInvalido = true;
          }

        

        } else {
          console.log("ERROR AL CONSULTAR USUARIOS FILTROS");
          this.passwordInvalido = true;
        }





      });
  }

}
