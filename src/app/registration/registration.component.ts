import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { NgbTimepicker, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import FileSaver from 'file-saver';
import { RestService } from '../providers/rest.service';
import * as XLSX from 'xlsx';
import { DatePipe } from '@angular/common';
import { UserService } from '../user.service';

class Registration {
  constructor(
    public idCONTROL_MATERIALES: string = null,
    public cliente: string[] = [null, null],
    public obra: string[] = [null, null],
    public proveedor: string[] = [null, null],
    public proveedort: string[] = [null, null],
    public material: string[] = [null, null],
    public origen: string = '',
    public destino: string = '',
    public placa: string = '',
    public unidad: string = null,
    public entrada: number = 0,
    public salida: number = 0,
    public cantidad: string = '',
    public fecha: NgbDateStruct = null,
    public horaEntrada = null,
    public horaSalida = null,
    public refProveedor: string = '',
    public precioUnidad : string = '0',
    public precio: string = '0',
    public usuario: string = ''
  ) { }
}

class Destinatario {
  constructor(
    public contabilidad: number = 1,
    public transportador: number = 0,
    public proveedor: number = 0,
    public despachador: number = 0,
    public maestro : number = 0
  ) { }
}


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  formControlMateriales: FormGroup;
  @ViewChild('modalClose') modalClose: ElementRef;

  // It maintains list of Registrations
  registrations: Registration[] = [];
  // It maintains registration Model
  regModel: Registration;

  regModelDestinatario:Destinatario;


  // It maintains registration form display status. By default it will be false.
  showNew: Boolean = false;
  showDestinatario: Boolean = false;
  // It will be either 'Save' or 'Update' based on operation.
  submitType: string = 'Save';
  // It maintains table row index based on selection.
  selectedRow: number;
  // It maintains Array of countries.
  clientes: string[];

  proveedores: string[];

  proveedorest: string[];

  obras: string[];

  materiales: string[];

  unidades: string[] = ['m', 'm2', 'm3'];

  selectedIndex: number;

  totalRec: number;

  page: number;




  //variables para validacion de campos manualmente despues de enviar formulario
  clienteValido: boolean = true;

  obraValido: boolean = true;

  materialValido: boolean = true;

  proveedorValido: boolean = true;

  proveedortValido: boolean = true;

  unidadValido: boolean = true;

  fechaValido: boolean = true;

  horaEntradaValido: boolean = true;

  horaSalidaValido: boolean = true;

  origenValido: boolean = true;

  destinoValido: boolean = true;

  placaValido: boolean = true;

  cantidadValido: boolean = true;

  refProveedorValido: boolean = true;

  guardo: boolean = true;

  private exportar = [];


  public fechaDesdeFiltro: NgbDateStruct = null;
  public fechaHastaFiltro: NgbDateStruct = null;
  public numeroReciboFiltro: String = null;

  constructor(public user: UserService, private restService: RestService, private cdRef: ChangeDetectorRef, private datePipe: DatePipe) {

    this.consultarClientes();
    this.consultarProveedores();
    this.consultarProveedorest();
    this.consultarObras();
    this.consultarMateriales();
    this.consultarControlMateriales();


  }

  consultarControlMaterialesFiltro(filtro) {

    this.restService.
      consultarControlMaterialesFiltros(filtro)
      .subscribe(data => {
        if (data.success === true) {

          this.exportar = [];
          this.registrations = [];

          console.log("CONTROL MATERIALES ENCONTRADO", data);

          var arrayControlMateriales = [];
          arrayControlMateriales = data.controlMateriales;

          this.totalRec = arrayControlMateriales.length;

          for (let controlMateriales of arrayControlMateriales) {
            console.log("CONTROL MATERIALES ENCONTRADO", controlMateriales);

            var cliente = [controlMateriales.cliente.idCLIENTE, controlMateriales.cliente.nomCLIENTE]
            var proveedor = [controlMateriales.proveedor.idPROVEEDOR, controlMateriales.proveedor.nomPROVEEDOR]
            var proveedort = [controlMateriales.proveedort.idPROVEEDORT, controlMateriales.proveedort.nomPROVEEDORT]
            var material = [controlMateriales.material.idMATERIAL, controlMateriales.material.nomMATERIAL]
            var obra = [controlMateriales.obra.idOBRA, controlMateriales.obra.nomOBRA]

            var fecha = new Date(controlMateriales.fecha);
            console.log('AQUI FECHA', fecha.toLocaleString());
            var fecha1 = fecha.toLocaleString();

            var horaEntrada = new Date(controlMateriales.horaEntrada);
            var horaSalida = new Date(controlMateriales.horaSalida);

            var fechaDateStruct: NgbDateStruct = { day: fecha.getUTCDate(), month: fecha.getUTCMonth() + 1, year: fecha.getUTCFullYear() };

            var horaEntradaRegModel = { hour: horaEntrada.getHours(), minute: horaEntrada.getMinutes() };
            var horaSalidaRegModel = { hour: horaSalida.getHours(), minute: horaSalida.getMinutes() };

            var registroExportar = {

              NUMERO_RECIBO: controlMateriales.idCONTROL_MATERIALES,
              FECHA: fecha1,
              HORA_ENTRADA: (horaEntradaRegModel.hour.toString().length>1?horaEntradaRegModel.hour:'0'+horaEntradaRegModel.hour)+ ':' + (horaEntradaRegModel.minute.toString().length>1?horaEntradaRegModel.minute:'0'+horaEntradaRegModel.minute),
              HORA_SALIDA: (horaSalidaRegModel.hour.toString().length>1?horaSalidaRegModel.hour:'0'+horaSalidaRegModel.hour) + ':' + (horaSalidaRegModel.minute.toString().length>1?horaSalidaRegModel.minute:'0'+horaSalidaRegModel.minute),
              CLIENTE: cliente[1],
              PROVEEDOR: proveedor[1],
              PROVEEDOR_TRANSPORTE: proveedort[1],
              OBRA: obra[1],
              ORIGEN: controlMateriales.origen,
              DESTINO: controlMateriales.destino,
              PLACA: controlMateriales.placa,
              MATERIAL: material[1],
              CANTIDAD: controlMateriales.cantidad,
              UNIDAD: controlMateriales.unidad,
              ENTRADA: controlMateriales.entrada == 1 ? 'SI' : 'NO',
              SALIDA: controlMateriales.salida == 1 ? 'SI' : 'NO',
              REF_PROVEEDOR: controlMateriales.refProveedor,
              PRECIO_UNIDAD : controlMateriales.precioUnidad,
              PRECIO_TOTAL: controlMateriales.precio,
              USUARIO: controlMateriales.usuario
            }

            this.exportar.push(registroExportar);

            this.registrations.push(new Registration(controlMateriales.idCONTROL_MATERIALES,
              cliente, obra, proveedor, proveedort, material, controlMateriales.origen, controlMateriales.destino, controlMateriales.placa, controlMateriales.unidad,
              controlMateriales.entrada, controlMateriales.salida, controlMateriales.cantidad, fechaDateStruct, horaEntradaRegModel, horaSalidaRegModel, controlMateriales.refProveedor, controlMateriales.precioUnidad, controlMateriales.precio, controlMateriales.usuario));
          }


        } else {
          console.log("ERROR AL OBTENER CONTROL MATERIALES");
          //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
        }
      });

  }

  consultarControlMateriales() {

    this.restService.
      consultarControlMateriales()
      .subscribe(data => {
        if (data.success === true) {
          this.exportar = [];
          this.registrations = [];

          console.log("CONTROL MATERIALES ENCONTRADO", data);

          var arrayControlMateriales = [];
          arrayControlMateriales = data.controlMateriales;

          this.totalRec = arrayControlMateriales.length;

          for (let controlMateriales of arrayControlMateriales) {
            console.log("CONTROL MATERIALES ENCONTRADO", controlMateriales);

            var cliente = [controlMateriales.cliente.idCLIENTE, controlMateriales.cliente.nomCLIENTE]
            var proveedor = [controlMateriales.proveedor.idPROVEEDOR, controlMateriales.proveedor.nomPROVEEDOR]
            var proveedort = [controlMateriales.proveedort.idPROVEEDORT, controlMateriales.proveedort.nomPROVEEDORT]
            var material = [controlMateriales.material.idMATERIAL, controlMateriales.material.nomMATERIAL]
            var obra = [controlMateriales.obra.idOBRA, controlMateriales.obra.nomOBRA]

            var fecha = new Date(controlMateriales.fecha);
            var fecha1 =  fecha.toLocaleString();
            console.log('AQUI FECHA', fecha1);

            var horaEntrada = new Date(controlMateriales.horaEntrada);
            var horaSalida = new Date(controlMateriales.horaSalida);

            var fechaDateStruct: NgbDateStruct = { day: fecha.getUTCDate(), month: fecha.getUTCMonth() + 1, year: fecha.getUTCFullYear() };

            var horaEntradaRegModel = { hour: horaEntrada.getHours(), minute: horaEntrada.getMinutes() };
            var horaSalidaRegModel = { hour: horaSalida.getHours(), minute: horaSalida.getMinutes() };



            var registroExportar = {

              NUMERO_RECIBO: controlMateriales.idCONTROL_MATERIALES,
              FECHA: fecha1,
              HORA_ENTRADA: (horaEntradaRegModel.hour.toString().length>1?horaEntradaRegModel.hour:'0'+horaEntradaRegModel.hour)+ ':' + (horaEntradaRegModel.minute.toString().length>1?horaEntradaRegModel.minute:'0'+horaEntradaRegModel.minute),
              HORA_SALIDA: (horaSalidaRegModel.hour.toString().length>1?horaSalidaRegModel.hour:'0'+horaSalidaRegModel.hour) + ':' + (horaSalidaRegModel.minute.toString().length>1?horaSalidaRegModel.minute:'0'+horaSalidaRegModel.minute),
              CLIENTE: cliente[1],
              PROVEEDOR: proveedor[1],
              PROVEEDOR_TRANSPORTE: proveedort[1],
              OBRA: obra[1],
              ORIGEN: controlMateriales.origen,
              DESTINO: controlMateriales.destino,
              PLACA: controlMateriales.placa,
              MATERIAL: material[1],
              CANTIDAD: controlMateriales.cantidad,
              UNIDAD: controlMateriales.unidad,
              ENTRADA: controlMateriales.entrada == 1 ? 'SI' : 'NO',
              SALIDA: controlMateriales.salida == 1 ? 'SI' : 'NO',
              REF_PROVEEDOR: controlMateriales.refProveedor,
              PRECIO_UNIDAD : controlMateriales.precioUnidad,
              PRECIO_TOTAL: controlMateriales.precio,
              USUARIO :controlMateriales.usuario
            }

            this.exportar.push(registroExportar);

            this.registrations.push(new Registration(controlMateriales.idCONTROL_MATERIALES,
              cliente, obra, proveedor, proveedort, material, controlMateriales.origen, controlMateriales.destino, controlMateriales.placa, controlMateriales.unidad,
              controlMateriales.entrada, controlMateriales.salida, controlMateriales.cantidad, fechaDateStruct, horaEntradaRegModel, horaSalidaRegModel, controlMateriales.refProveedor,controlMateriales.precioUnidad, controlMateriales.precio,controlMateriales.usuario));
          }


        } else {
          console.log("ERROR AL OBTENER CONTROL MATERIALES");
          //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
        }
      });

  }

  consultarClientes() {

    this.restService.
      consultarClientes()
      .subscribe(data => {
        if (data.success === true) {

          this.clientes = data.clientes;



        } else {
          console.log("ERROR AL OBTENER CLIENTES");
          //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
        }
      });

  }


  consultarProveedores() {

    this.restService.
      consultarProveedores()
      .subscribe(data => {
        if (data.success === true) {

          this.proveedores = data.proveedores;

        } else {
          console.log("ERROR AL OBTENER PROVEEDORES");
          //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
        }
      });

  }


  consultarProveedorest() {

    this.restService.
      consultarProveedorest()
      .subscribe(data => {
        if (data.success === true) {

          this.proveedorest = data.proveedorest;

        } else {
          console.log("ERROR AL OBTENER PROVEEDOREST");
          //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
        }
      });

  }

  consultarObras() {

    this.restService.
      consultarObras()
      .subscribe(data => {
        if (data.success === true) {

          this.obras = data.obras;

        } else {
          console.log("ERROR AL OBTENER OBRAS");
          //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
        }
      });

  }

  consultarMateriales() {

    this.restService.
      consultarMateriales()
      .subscribe(data => {
        if (data.success === true) {

          this.materiales = data.materiales;

        } else {
          console.log("ERROR AL OBTENER MATERIALES");
          //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
        }
      });

  }

  ngOnInit() {
    this.formControlMateriales = new FormGroup({
      'idCONTROL_MATERIALES': new FormControl({ value: null, disabled: true }),
      'cliente': new FormControl('null', Validators.compose([Validators.required, Validators.pattern('.*[^ ].*')])),
      'proveedor': new FormControl('null', Validators.compose([Validators.required, Validators.pattern('.*[^ ].*')])),
      'proveedort': new FormControl('null', Validators.compose([Validators.required, Validators.pattern('.*[^ ].*')])),
      'obra': new FormControl('null', Validators.compose([Validators.required, Validators.pattern('.*[^ ].*')])),
      'material': new FormControl('null', Validators.compose([Validators.required, Validators.pattern('.*[^ ].*')])),
      'unidad': new FormControl('null', Validators.compose([Validators.required, Validators.pattern('.*[^ ].*')])),
      'origen': new FormControl(null, Validators.compose([Validators.required, Validators.pattern('.*[^ ].*')])),
      'destino': new FormControl(null, Validators.compose([Validators.required, Validators.pattern('.*[^ ].*')])),
      'placa': new FormControl(null, Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z]{3}[0-9]{3}$')])),
      'cantidad': new FormControl(null, Validators.compose([Validators.required, Validators.pattern('.*[^ ].*')])),
      'fecha': new FormControl(null, Validators.required),
      'horaEntrada': new FormControl(null, Validators.required),
      'horaSalida': new FormControl(null, Validators.required),
      'refProveedor': new FormControl(null, Validators.compose([Validators.required, Validators.pattern('.*[^ ].*')]))
    });

  }

  validarCampos() {

    if (!this.formControlMateriales.controls['cliente'].valid) {
      this.clienteValido = false;
    };

    if (!this.formControlMateriales.controls['obra'].valid) {
      this.obraValido = false;
    };

    if (!this.formControlMateriales.controls['proveedor'].valid) {
      this.proveedorValido = false;
    };

    if (!this.formControlMateriales.controls['proveedort'].valid) {
      this.proveedortValido = false;
    };

    if (!this.formControlMateriales.controls['unidad'].valid) {
      this.unidadValido = false;
    };

    if (!this.formControlMateriales.controls['fecha'].valid) {
      this.fechaValido = false;
    };

    if (!this.formControlMateriales.controls['material'].valid) {
      this.materialValido = false;
    };

    if (!this.formControlMateriales.controls['horaEntrada'].valid) {
      this.horaEntradaValido = false;
    };

    if (!this.formControlMateriales.controls['horaSalida'].valid) {
      this.horaSalidaValido = false;
    };

    if (!this.formControlMateriales.controls['origen'].valid) {
      this.origenValido = false;
    };

    if (!this.formControlMateriales.controls['destino'].valid) {
      this.destinoValido = false;
    };

    if (!this.formControlMateriales.controls['placa'].valid) {
      this.placaValido = false;
    };

    if (!this.formControlMateriales.controls['cantidad'].valid) {
      this.cantidadValido = false;
    };

    if (!this.formControlMateriales.controls['refProveedor'].valid) {
      this.refProveedorValido = false;
    };




  }

  // This method associate to New Button.
  onNew() {
    // Initiate new registration.
    this.guardo = true;
    this.regModel = new Registration();
    this.regModelDestinatario = new Destinatario();
    var fechaHora = new Date();
    console.log('AQUI FECHA', fechaHora);


    var fechaDateStruct: NgbDateStruct = { day: fechaHora.getUTCDate(), month: fechaHora.getUTCMonth() + 1, year: fechaHora.getUTCFullYear() };

    var horaEntradaRegModel = { hour: fechaHora.getHours(), minute: fechaHora.getMinutes() };
    var horaSalidaRegModel = { hour: fechaHora.getHours(), minute: fechaHora.getMinutes() };

    this.regModel.fecha = fechaDateStruct;
    this.regModel.horaSalida = horaSalidaRegModel;

    this.formControlMateriales.reset();
    this.limpiarValidaciones();
    // Change submitType to 'Save'.
    this.submitType = 'Save';
    // display registration entry section.
    this.showNew = true;
    this.cdRef.detectChanges();
  }

  limpiarValidaciones() {

    this.clienteValido = true;
    this.obraValido = true;
    this.materialValido = true;
    this.proveedorValido = true;
    this.proveedortValido = true;
    this.unidadValido = true;
    this.fechaValido = true;
    this.horaEntradaValido = true;
    this.horaSalidaValido = true;
    this.origenValido = true;
    this.destinoValido = true;
    this.placaValido = true;
    this.cantidadValido = true;
    this.refProveedorValido = true;
  }

  filtrar() {
    var filtros = {}
    var filtrar = false;
    if (this.fechaDesdeFiltro != null) {
      filtros['fechaDesde'] = new Date(this.fechaDesdeFiltro.year + '-' + this.fechaDesdeFiltro.month + '-' + this.fechaDesdeFiltro.day);
      filtrar = true;
    }

    if (this.fechaDesdeFiltro != null) {
      filtros['fechaHasta'] = new Date(this.fechaHastaFiltro.year + '-' + this.fechaHastaFiltro.month + '-' + this.fechaHastaFiltro.day);
      filtrar = true;
    }
    if (this.numeroReciboFiltro != null && this.numeroReciboFiltro.length > 0) {
      filtros['idCONTROL_MATERIALES'] = this.numeroReciboFiltro;
      filtrar = true
    }

    if (filtrar == true) {
      this.consultarControlMaterialesFiltro(filtros);
    } else {
      this.consultarControlMateriales();
    }



  }

  // This method associate to Save Button.
  onSave() {

    this.validarCampos();


    if (this.formControlMateriales.valid) {
      var promises = [];

      promises.push(

        new Promise((resolve, reject) => {
          var obraProveedorest = [];

          var filtro = {

            idOBRA: this.regModel.obra[0],
            idPROVEEDORT: this.regModel.proveedort[0]

          }


          this.restService.
            consultarObraProveedorestFiltros(filtro)
            .subscribe(data => {
              if (data.success === true) {

                var arrayObraProveedorest = [];
                arrayObraProveedorest = data.obraProveedorest;


                if (arrayObraProveedorest[0] != null) {
                  var obraProveedort = arrayObraProveedorest[0];

                  var precio = parseInt(this.regModel.cantidad) * parseInt(obraProveedort.precio);

                  this.regModel.precio = precio.toString();

                  this.regModel.precioUnidad = obraProveedort.precio;

                  console.log("PRECIO IMPRIMIR--->" + this.regModel.precio);

                  resolve();

                } else {
                  resolve();
                }


              } else {
                console.log("ERROR AL OBTENER OBRAPROVEEDORTS PRECIO");

                resolve();
                //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
              }
            });
        })


      )

      Promise.all(promises).then((values) => {


        if (this.submitType === 'Save') {
          // Push registration model object into registration list.
          //this.registrations.push(this.regModel);

          var datos = this.obtenerArregloDatosInsert();

          this.restService.
            insertarControlMateriales(datos)
            .subscribe(data => {
              if (data.success === true) {
                this.regModel.idCONTROL_MATERIALES = data.controlMateriales.idCONTROL_MATERIALES;
                this.consultarControlMateriales();



              } else {
                console.log("ERROR AL INSERTAR MATERIALS");
                //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
              }
            });




        } else {
          // Update the existing properties values based on model.

          var datos = this.obtenerArregloDatosInsert();

          this.restService.
            editarControlMateriales(this.regModel.idCONTROL_MATERIALES, datos)
            .subscribe(data => {
              if (data.success === true) {

                this.consultarControlMateriales();

              } else {
                console.log("ERROR AL INSERTAR MATERIALS");
                //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
              }
            });


        }
        //this.showNew = false;
        //this.modalClose.nativeElement.click();
        this.guardo = false;

      });

    }
  }

  obtenerArregloDatosInsert() {

    var horaEntrada = new Date();
    horaEntrada.setFullYear(this.regModel.fecha.year);
    horaEntrada.setMonth(this.regModel.fecha.month);
    horaEntrada.setDate(this.regModel.fecha.day);
    horaEntrada.setHours(this.regModel.horaEntrada.hour);
    horaEntrada.setMinutes(this.regModel.horaEntrada.minute);
    var horaEntradaFormat = this.datePipe.transform(horaEntrada, 'yyyy-MM-dd HH:mm:ss');

    var horaSalida = new Date();
    horaSalida.setFullYear(this.regModel.fecha.year);
    horaSalida.setMonth(this.regModel.fecha.month);
    horaSalida.setDate(this.regModel.fecha.day);
    horaSalida.setHours(this.regModel.horaSalida.hour);
    horaSalida.setMinutes(this.regModel.horaSalida.minute);
    var horaSalidaFormat = this.datePipe.transform(horaSalida, 'yyyy-MM-dd HH:mm:ss');
    var fecha = this.regModel.fecha.year + '-' + this.regModel.fecha.month + '-' + this.regModel.fecha.day;

    this.regModel.usuario = this.user.login;

    var datos = {

      idCLIENTE: this.regModel.cliente[0],
      idPROVEEDOR: this.regModel.proveedor[0],
      idPROVEEDORT: this.regModel.proveedort[0],
      idMATERIAL: this.regModel.material[0],
      idOBRA: this.regModel.obra[0],
      unidad: this.regModel.unidad,
      entrada: this.regModel.entrada,
      salida: this.regModel.salida,
      cantidad: this.regModel.cantidad,
      origen: this.regModel.origen,
      destino: this.regModel.destino,
      refProveedor: this.regModel.refProveedor,
      fecha: fecha,
      horaEntrada: horaEntradaFormat,
      horaSalida: horaSalidaFormat,
      placa: this.regModel.placa,
      usuario: this.regModel.usuario,
      precio: this.regModel.precio,
      precioUnidad: this.regModel.precioUnidad

    }

    return datos;

  }

  // This method associate to Edit Button.
  onEdit(index: number) {
    // Assign selected table row index.
    this.selectedRow = index;
    // Initiate new registration.
    this.regModel = new Registration();
    this.regModelDestinatario = new Destinatario();
    // Retrieve selected registration from list and assign to model.
    this.regModel = Object.assign({}, this.registrations[this.selectedRow]);
    // Change submitType to Update.
    this.submitType = 'Update';
    // Display registration entry section.
    this.showNew = true;
  }

  // This method associate to Delete Button.
  // This method associate to Delete Button.
  onDelete(index: number) {
    // Delete the corresponding material entry from the list.
    console.log("AQUI CONFIRMAR ELIMINACION", index);
    this.restService.
      eliminarControlMateriales(this.registrations[index].idCONTROL_MATERIALES)
      .subscribe(data => {
        if (data.success === true) {
          this.registrations.splice(index, 1);
        } else {
          console.log("ERROR AL ELIMINAR MATERIALS");
          //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
        }
      });


  }

  // This method associate toCancel Button.
  onCancel() {
    // Hide registration entry section.
    this.showNew = false;
  }

  // This method associate to Bootstrap dropdown selection change.
  onChangeCliente(cliente) {
    // Assign corresponding selected country to model.
    this.regModel.cliente[0] = cliente.idCLIENTE;
    this.regModel.cliente[1] = cliente.nomCLIENTE;
  }

  // This method associate to Bootstrap dropdown selection change.
  onChangeObra(obra) {
    // Assign corresponding selected country to model.
    this.regModel.obra[0] = obra.idOBRA;
    this.regModel.obra[1] = obra.nomOBRA;
  }

  // This method associate to Bootstrap dropdown selection change.
  onChangeMaterial(material) {
    // Assign corresponding selected country to model.
    this.regModel.material[0] = material.idMATERIAL;
    this.regModel.material[1] = material.nomMATERIAL;
  }

  onChangeProveedor(proveedor) {
    // Assign corresponding selected country to model.
    this.regModel.proveedor[0] = proveedor.idPROVEEDOR;
    this.regModel.proveedor[1] = proveedor.nomPROVEEDOR;
  }

  onChangeProveedort(proveedort) {
    // Assign corresponding selected country to model.
    this.regModel.proveedort[0] = proveedort.idPROVEEDORT;
    this.regModel.proveedort[1] = proveedort.nomPROVEEDORT;
  }

  onChangeUnidad(unidad) {
    // Assign corresponding selected country to model.
    this.regModel.unidad = unidad;
  }

  checkEntrada(checked) {

    if (checked) {
      this.regModel.entrada = 1;
      this.regModel.salida = 0;
    } else {
      this.regModel.entrada = 0;
      this.regModel.salida = 1;
    }
  }

  checkSalida(checked) {

    if (checked) {
      this.regModel.salida = 1;
      this.regModel.entrada = 0;
    } else {
      this.regModel.salida = 0;
      this.regModel.entrada = 1;
    }
  }


  checkContabilidad(checked) {

    if (checked) {
      this.regModelDestinatario.contabilidad = 1;
      this.regModelDestinatario.transportador = 0;
      this.regModelDestinatario.proveedor = 0;
      this.regModelDestinatario.despachador = 0;
      this.regModelDestinatario.maestro=0;
    } else {
      this.regModelDestinatario.contabilidad = 0;
    }
  }

  checkProveedor(checked) {

    if (checked) {
      this.regModelDestinatario.proveedor = 1;
      this.regModelDestinatario.maestro=0;
      this.regModelDestinatario.transportador = 0;
      this.regModelDestinatario.contabilidad = 0;
      this.regModelDestinatario.despachador = 0;
    } else {
      this.regModelDestinatario.proveedor = 0;
    }
  }

  checkTransportador(checked) {

    if (checked) {
      this.regModelDestinatario.transportador = 1;
      this.regModelDestinatario.proveedor = 0;
      this.regModelDestinatario.maestro=0;
      this.regModelDestinatario.contabilidad = 0;
      this.regModelDestinatario.despachador = 0;
    } else {
      this.regModelDestinatario.transportador = 0;
    }
  }

  checkDespachador(checked) {

    if (checked) {
      this.regModelDestinatario.despachador = 1;
      this.regModelDestinatario.proveedor = 0;
      this.regModelDestinatario.contabilidad = 0;
      this.regModelDestinatario.maestro=0;
      this.regModelDestinatario.transportador = 0;
    } else {
      this.regModelDestinatario.despachador = 0;
    }
  }

  checkMaestro(checked) {

    if (checked) {
      this.regModelDestinatario.maestro = 1;
      this.regModelDestinatario.proveedor = 0;
      this.regModelDestinatario.contabilidad = 0;
      this.regModelDestinatario.despachador=0;
      this.regModelDestinatario.transportador = 0;
    } else {
      this.regModelDestinatario.maestro = 0;
    }
  }

  // This method associate to Delete Button.
  confirmDelete(index: number) {
    console.log("AQUI CONFIRMAR ELIMINACION", index);
    this.selectedIndex = index;
  }


  seleccionarDestinatario(){


    this.showDestinatario = true;


  }


  generarRecibo() {

    var destinatario = '';
    var destinatarioFileName = '';

    if (this.regModelDestinatario.contabilidad==1){

      destinatario = "Original: Contabilidad";
      destinatarioFileName = "Contabilidad";

    }else if (this.regModelDestinatario.proveedor==1){

      destinatario = "Copia: Proveedor/Obra"
      destinatarioFileName = "Proveedor";

    }else if (this.regModelDestinatario.transportador==1){

      destinatario = "Copia: Transportador"
      destinatarioFileName = "Transportador";

    }else if (this.regModelDestinatario.despachador==1){

      destinatario = "Copia: Despachador/Recibidor"
      destinatarioFileName = "Despachador";
    }
    else if (this.regModelDestinatario.maestro==1){

      destinatario = "Copia: Maestro"
      destinatarioFileName = "Maestro"

    }


    var horaEntrada = new Date();
    horaEntrada.setFullYear(this.regModel.fecha.year);
    horaEntrada.setMonth(this.regModel.fecha.month);
    horaEntrada.setDate(this.regModel.fecha.day);
    horaEntrada.setHours(this.regModel.horaEntrada.hour);
    horaEntrada.setMinutes(this.regModel.horaEntrada.minute);
    var horaEntradaFormat = this.datePipe.transform(horaEntrada, 'yyyy-MM-dd HH:mm:ss');

    var horaSalida = new Date();
    horaSalida.setFullYear(this.regModel.fecha.year);
    horaSalida.setMonth(this.regModel.fecha.month);
    horaSalida.setDate(this.regModel.fecha.day);
    horaSalida.setHours(this.regModel.horaSalida.hour);
    horaSalida.setMinutes(this.regModel.horaSalida.minute);
    var horaSalidaFormat = this.datePipe.transform(horaSalida, 'yyyy-MM-dd HH:mm:ss');

    var horaEntradaRegModel = { hour: horaEntrada.getHours(), minute: horaEntrada.getMinutes() };
    var horaSalidaRegModel = { hour: horaSalida.getHours(), minute: horaSalida.getMinutes() };
    var fecha = this.regModel.fecha.day + '/' + this.regModel.fecha.month + '/' + this.regModel.fecha.year;


    var arrayExport = [];

    arrayExport[0] = "<html><head></head><body><print>";
    arrayExport[1] = "       COBICIVIL S.A.S";
    arrayExport[2] = "www.cobicivil.com - PBX: 5586151";
    arrayExport[3] = "--------------------------------";
    arrayExport[4] = "CONTROL MATERIALES";
    arrayExport[5] = "Recibo No.        " + this.regModel.idCONTROL_MATERIALES;
    arrayExport[6] = "Fecha:            " + fecha;
    arrayExport[7] = "Hora Entrada:     " + (horaEntradaRegModel.hour.toString().length>1?horaEntradaRegModel.hour:'0'+horaEntradaRegModel.hour)+ ':' + (horaEntradaRegModel.minute.toString().length>1?horaEntradaRegModel.minute:'0'+horaEntradaRegModel.minute);
    arrayExport[8] = "Hora Salida:      " + (horaSalidaRegModel.hour.toString().length>1?horaSalidaRegModel.hour:'0'+horaSalidaRegModel.hour) + ':' + (horaSalidaRegModel.minute.toString().length>1?horaSalidaRegModel.minute:'0'+horaSalidaRegModel.minute);
    arrayExport[9] = "Cliente:    " + this.regModel.cliente[1];
    arrayExport[10] = "Proveedor:  " + this.regModel.proveedor[1];
    arrayExport[11] = "Prov. Transporte: " + this.regModel.proveedort[1];
    arrayExport[12] = "Ref. Proveedor:   " + this.regModel.refProveedor;
    arrayExport[13] = "Obra:     " + this.regModel.obra[1];
    arrayExport[14] = "Material: " + this.regModel.material[1];
    arrayExport[15] = "Cantidad: " + this.regModel.cantidad;
    arrayExport[16] = "Unidad:   " + this.regModel.unidad;
  
    arrayExport[17] = "Placa:    " + this.regModel.placa;
    arrayExport[18] = "Origen:   " + this.regModel.origen;
    arrayExport[19] = "Destino:  " + this.regModel.destino;
    arrayExport[20] = "Entrada:  " + this.regModel.entrada;
    arrayExport[21] = "Salida:   " + this.regModel.salida;
    arrayExport[22] = "--------------------------------";
    arrayExport[arrayExport.length] = "Usuario:   " + this.regModel.usuario;
    if (this.regModelDestinatario.maestro==0){
    arrayExport[arrayExport.length] = "Precio Unit.:  $ " + this.regModel.precioUnidad+".00";;
    arrayExport[arrayExport.length] = "Precio Total:  $ " + this.regModel.precio+".00";
    }


    arrayExport[arrayExport.length] = "Firma responsable:";
    arrayExport[arrayExport.length] = "<br>";
    arrayExport[arrayExport.length] = "<br>";
    arrayExport[arrayExport.length] = "________________________________";
    arrayExport[arrayExport.length] = destinatario;
    arrayExport[arrayExport.length] = new Date().toLocaleString();
    arrayExport[arrayExport.length] = "<br>";
    arrayExport[arrayExport.length] = "<br>";
    arrayExport[arrayExport.length] = "</print></body></html>";

    this.regModel.entrada == 1 ? arrayExport[20] = "Entrada:  SI" : arrayExport[20] = "Entrada:  NO";
    this.regModel.salida == 1 ? arrayExport[21] = "Salida:   SI" : arrayExport[21] = "Salida:   NO";



    var data = JSON.stringify(arrayExport);

    var res = data.replace(/['"]+/g, '');

    res = res.replace(/,/g, "\n");

    res = res.replace("[", '');

    res = res.replace("]", '');


    var blob = new Blob([res], { type: "text/html;charset=utf-8" });
    FileSaver.saveAs(blob, "Materiales_Recibo_" + this.regModel.idCONTROL_MATERIALES+"_"+destinatarioFileName);


  }





  exportarExcel() {

    /* make the worksheet */
    var ws = XLSX.utils.json_to_sheet(this.exportar);

    /* add to workbook */
    var wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "People");

    /* write workbook (use type 'binary') */
    var wbout = XLSX.write(wb, { bookType: 'xlsx', type: 'binary' });

    var today = new Date();
    var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
    var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
    var name = "reporteControlMateriales" + date + time;

    FileSaver.saveAs(new Blob([this.s2ab(wbout)], { type: "application/octet-stream" }), name + ".xlsx");
  }

  s2ab(s) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: "application/octet-stream"
    });
    var today = new Date();
    var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
    var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
    var name = fileName + date + time;
    FileSaver.saveAs(data, name + ".xlsx");
  }




}
