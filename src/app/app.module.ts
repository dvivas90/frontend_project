import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { RegistrationComponent } from './registration/registration.component';
import { ObraComponent } from './obra/obra.component';
import { ClienteComponent } from './cliente/cliente.component';
import { ActividadComponent } from './actividad/actividad.component';
import { EquipoComponent } from './equipo/equipo.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { MaterialComponent } from './material/material.component';
import { ProveedortComponent } from './proveedort/proveedort.component';
import { ObraProveedortComponent } from './obraProveedort/obraProveedort.component';
import {DatePipe} from '@angular/common';
import { ProveedorComponent } from './proveedor/proveedor.component';
import { PropietarioComponent } from './propietario/propietario.component';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import { LoginformComponent } from './loginform/loginform.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import {UserService} from './user.service';
import {DataService} from './providers/data.service';
import {RestService} from './providers/rest.service';
import { ConfigurationProvider } from './providers/configuration';
import { ControlEquiposComponent } from './control-equipos/control-equipos.component';
import {HttpModule} from '@angular/http';
import {NgxPaginationModule} from 'ngx-pagination';
//import { DataTableModule } from 'angular-4-data-table-fix';



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    RegistrationComponent,
    ObraComponent,
    ClienteComponent,
    ActividadComponent,
    ProveedortComponent,
    EquipoComponent,
    MaterialComponent,
    UsuarioComponent,
    ProveedorComponent,
    LoginformComponent,
    PropietarioComponent,
    HeaderComponent,
    FooterComponent,
    ControlEquiposComponent,
    ObraProveedortComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    FormsModule,
    HttpModule,
    NgxPaginationModule,
   // DataTableModule,
    ReactiveFormsModule
  ],
  providers: [
    UserService,
    DataService,
    RestService,
    ConfigurationProvider,
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
