import { Component, OnInit,ViewChild, ElementRef} from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import FileSaver from 'file-saver';
import { RestService} from '../providers/rest.service';
import * as XLSX from 'xlsx';

class Proveedor {
  constructor(
    public idPROVEEDOR: string = '',
    public nomPROVEEDOR: string = '',
    public desPROVEEDOR: string = ''
  ) {}
}

@Component({
  selector: 'app-proveedor',
  templateUrl: './proveedor.component.html',
  styleUrls: ['./proveedor.component.css']
})
export class ProveedorComponent implements OnInit {
  formProveedor: FormGroup;
  @ViewChild('modalClose') modalClose:ElementRef;
  
  // It maintains list of proveedores
  proveedores: Proveedor[] = [];
  // It maintains proveedor Model
  regModel: Proveedor;
  // It maintains proveedor form display status. By default it will be false.
  showNew: Boolean = false;
  // It will be either 'Save' or 'Update' based on operation.
  submitType: string = 'Save';
  // It maintains table row index based on selection.
  selectedRow: number;
  // It maintains Array of countries.
  totalRec : number;
  page: number = 1;

  selectedIndex : number;

  public filtroNombre: String = null;

  constructor(private restService:RestService) {
    
    this.consultarProveedores();

  }

  consultarProveedores(){
    // Add default proveedor data.
    this.proveedores = [];

    this.restService.
      consultarProveedores()
        .subscribe(data => {
          if (data.success === true) {

            var arrayProveedores = [];
            arrayProveedores = data.proveedores;

            this.totalRec = arrayProveedores.length;

            for (let proveedor of arrayProveedores) {
              console.log("PROVEEDOR ENCONTRADA",proveedor);
              this.proveedores.push(new Proveedor(proveedor.idPROVEEDOR,proveedor.nomPROVEEDOR, proveedor.desPROVEEDOR));
           }
          
  
          } else {
            console.log("ERROR AL OBTENER PROVEEDORS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });
  }

  ngOnInit() {
    this.formProveedor = new FormGroup({
      'idPROVEEDOR': new FormControl({value:null,disabled: true}),
      'nomPROVEEDOR': new FormControl(null, Validators.compose([Validators.required,Validators.pattern(".*[^ ].*")])), 
      'desPROVEEDOR': new FormControl(null, Validators.compose([Validators.required,Validators.pattern(".*[^ ].*")]))
		});
    
  }

  // This method associate to New Button.
  onNew() {
    // Initiate new proveedor.
    this.regModel = new Proveedor();
    this.formProveedor.reset();
    // Change submitType to 'Save'.
    this.submitType = 'Save';
    // display proveedor entry section.
    this.showNew = true;
  }

  filtrar(){
    var date = new Date();

    if (this.filtroNombre!=null && this.filtroNombre!=''){

      var filtros = { 
          nomPROVEEDOR:this.filtroNombre

      }

      this.restService.
       consultarProveedoresFiltros(filtros)
        .subscribe(data => {
          if (data.success === true) {

            this.proveedores = data.proveedores;

            this.totalRec = this.proveedores.length;
          
          } else {
            console.log("ERROR AL CONSULTAR PROVEEDORS FILTROS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });

    }else{
      this.consultarProveedores();
    }

    
  
  }

  // This method associate to Save Button.
  onSave() {
    
    if(this.formProveedor.valid){

    if (this.submitType === 'Save') {
      // Push proveedor model object into proveedor list.

      var proveedor = {
        nomPROVEEDOR:this.regModel.nomPROVEEDOR.trim(),
        desPROVEEDOR:this.regModel.desPROVEEDOR.trim()
      }
      this.restService.
       insertarProveedor(proveedor)
        .subscribe(data => {
          if (data.success === true) {

            this.proveedores.push(data.proveedor);
          
          } else {
            console.log("ERROR AL INSERTAR PROVEEDORS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });


    } else {
      // Update the existing properties values based on model.

      this.restService.
       editarProveedor(this.regModel.idPROVEEDOR,this.regModel)
        .subscribe(data => {
          if (data.success === true) {
          this.proveedores[this.selectedRow].idPROVEEDOR = this.regModel.idPROVEEDOR;
          this.proveedores[this.selectedRow].nomPROVEEDOR = this.regModel.nomPROVEEDOR;
          this.proveedores[this.selectedRow].desPROVEEDOR = this.regModel.desPROVEEDOR;
          } else {
            console.log("ERROR AL EDITAR PROVEEDORS");
          }
        });
    }
    // Hide proveedor entry section.
    this.showNew = false;
    this.modalClose.nativeElement.click();
    }
  }

  // This method associate to Edit Button.
  onEdit(index: number) {
    // Assign selected table row index.
    this.selectedRow = index;
    // Initiate new proveedor.
    this.regModel = new Proveedor();

    console.log('PROVEEDOR EDITAR',this.proveedores[this.selectedRow])
    // Retrieve selected proveedor from list and assign to model.
    this.regModel = Object.assign({}, this.proveedores[this.selectedRow]);
    // Change submitType to Update.
    this.submitType = 'Update';
    // Display proveedor entry section.
    this.showNew = true;
  }

  // This method associate to Delete Button.
  confirmDelete(index: number) {
    console.log("AQUI CONFIRMAR ELIMINACION",index);
    this.selectedIndex = index;
  }

  // This method associate to Delete Button.
  onDelete(index: number) {
    // Delete the corresponding proveedor entry from the list.
    console.log("AQUI CONFIRMAR ELIMINACION",index);
    this.restService.
      eliminarProveedor(this.proveedores[index].idPROVEEDOR)
        .subscribe(data => {
          if (data.success === true) {
            this.proveedores.splice(index,1);
          } else {
            console.log("ERROR AL ELIMINAR PROVEEDORS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });
    
    
  }

  // This method associate toCancel Button.
  onCancel() {
    // Hide proveedor entry section.
    this.showNew = false;
  }

 

  exportarExcel(){

    /* make the worksheet */
    var ws = XLSX.utils.json_to_sheet(this.proveedores);

    /* add to workbook */
    var wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "People");

    /* write workbook (use type 'binary') */
    var wbout = XLSX.write(wb, {bookType:'xlsx', type:'binary'});

    var today = new Date();
    var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
    var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
    var name = "reporteProveedor" + date + time;

    FileSaver.saveAs(new Blob([this.s2ab(wbout)],{type:"application/octet-stream"}), name+".xlsx");
  }

   s2ab(s) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
  }

   exportAsExcelFile(json: any[], excelFileName: string): void {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }

    private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
          type: "application/octet-stream"
        });
        var today = new Date();
        var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
        var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
        var name = fileName + date + time;
        FileSaver.saveAs(data, name + ".xlsx");
}




}
