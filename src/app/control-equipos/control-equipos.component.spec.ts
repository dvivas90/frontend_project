import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlEquiposComponent } from './control-equipos.component';

describe('ControlEquiposComponent', () => {
  let component: ControlEquiposComponent;
  let fixture: ComponentFixture<ControlEquiposComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlEquiposComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlEquiposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
