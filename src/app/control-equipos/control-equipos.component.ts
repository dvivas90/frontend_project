import { Component, OnInit,ViewChild, ElementRef,ChangeDetectorRef} from '@angular/core';
import {NgbTimepicker, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import FileSaver from 'file-saver';
import { RestService} from '../providers/rest.service';
import * as XLSX from 'xlsx';
import { DatePipe } from '@angular/common';
import { UserService } from '../user.service';

class ReporteEquipos {
  constructor(
    public idREPORTE_EQUIPO: string = null,
    public equipo: string[] = [null,null,null],
    public obra: string[] = [null,null],
    public propietario: string[] = [null,null],
    public propio:number=0,
    public alquilado:number=0,
    public totalHoras:number=0,
    public fecha: NgbDateStruct = null,
    public observaciones: string = '',
    public precio : string = '0',
    public usuario : string = ''
    
  ) {}
}

class DetReporteEquipos {
  constructor(
    public idDET_REPORTE_EQUIPO: string = null,
    public idREPORTE_EQUIPO:  string = null,
    public horas:number=0,
    public actividad: string []= [null,null],
    public horaDesde  = null,
    public horaHasta  = null,
    
  ) {}
}

@Component({
  selector: 'app-control-equipos',
  templateUrl: './control-equipos.component.html',
  styleUrls: ['./control-equipos.component.css']
})
export class ControlEquiposComponent implements OnInit {
  formReporteEquipo: FormGroup;
  @ViewChild('modalClose') modalClose:ElementRef;
  @ViewChild('modalCloseDet') modalCloseDet:ElementRef;
  
  // It maintains list of ControlEquiposs
  reporteEquipos: ReporteEquipos[] = [];
  // It maintains control-equipos Model
  regModel: ReporteEquipos;
  // It maintains control-equipos form display status. By default it will be false.
  showNew: Boolean = false;
  // It will be either 'Save' or 'Update' based on operation.
  submitType: string = 'Save';
  // It maintains table row index based on selection.
  selectedRow: number;
  // It maintains Array of countries.
  equipos: string[] ;

  propietarios: string[] ;

  obras: string[] ;

  actividades: string[] ;

  selectedIndex : number;

  totalRec : number;



  //variables para validacion de campos manualmente despues de enviar formulario
  equipoValido : boolean = true;

  obraValido : boolean = true;

  propietarioValido : boolean = true;

  fechaValido : boolean = true;

  totalHorasValido : boolean = true;

  observacionesValido : boolean = true;

  guardo : boolean = true;


  private exportar = [];


  public fechaDesdeFiltro: NgbDateStruct = null;
  public fechaHastaFiltro: NgbDateStruct = null;
  public numeroReciboFiltro: String = null;

  //DETALLE REPORTE EQUIPO

  detReporteEquipos: DetReporteEquipos[] = [];

  horaDesdeValido : boolean = true;

  horaHastaValido : boolean = true;

  actividadValido : boolean = true;

  detRegModel: DetReporteEquipos;

  formDetReporteEquipo: FormGroup;

  showNewDet: Boolean = false

  showVerDet: Boolean = false

  submitTypeDet: string = 'Save';

  totalRecDet : number;

  page:number;

  selectedRowDet: number;

  selectedIndexDet : number;


  constructor(private user:UserService, private restService:RestService,private cdRef:ChangeDetectorRef,private datePipe:DatePipe) {

    this.consultarEquipos();
    this.consultarPropietarios();
    this.consultarObras();
    this.consultarReporteEquipo();
    this.consultarActividades();
    
    
  }




  consultarReporteEquipoFiltros(filtro){

    this.restService.
    consultarReporteEquipoFiltros(filtro)
      .subscribe(data => {
        if (data.success === true) {

          this.exportar = [];
          this.reporteEquipos = [];

          console.log("REPORTE EQUIPO ENCONTRADO",data);

            var arrayReporteEquipo= [];
            arrayReporteEquipo = data.reporteEquipos;

            this.totalRec = arrayReporteEquipo.length;

            for (let reporteEquipo of arrayReporteEquipo) {
              console.log("REPORTE EQUIPO ENCONTRADO",reporteEquipo);

              var equipo = [reporteEquipo.equipo.idEQUIPO,reporteEquipo.equipo.nomEQUIPO,reporteEquipo.equipo.precio ]
              var propietario = [reporteEquipo.propietario.idPROPIETARIO,reporteEquipo.propietario.nomPROPIETARIO ]
              var obra = [reporteEquipo.obra.idOBRA,reporteEquipo.obra.nomOBRA ]

              var fecha = new Date(reporteEquipo.fecha);
              console.log('AQUI FECHA',fecha);

              var fechaDateStruct: NgbDateStruct = { day: fecha.getUTCDate(), month: fecha.getUTCMonth() + 1, year: fecha.getUTCFullYear()};

              var registroExportar={

                NUMERO_RECIBO :reporteEquipo.idREPORTE_EQUIPO,
                FECHA: fecha,
                EQUIPO : equipo[1],
                PRECIO_HORA : equipo[2],
                PRECIO_TOTAL : reporteEquipo.precio,
                PROPIETARIO : propietario[1],
                OBRA: obra[1],
                TOTAL_HORAS: reporteEquipo.totalHoras,
                PROPIO: reporteEquipo.propio==1?'SI':'NO',
                ALQUILADO: reporteEquipo.alquilado==1?'SI':'NO',
                USUARIO : reporteEquipo.usuario

              }

              this.exportar.push(registroExportar);

              this.reporteEquipos.push(new ReporteEquipos(reporteEquipo.idREPORTE_EQUIPO,
                equipo,obra,propietario,reporteEquipo.propio,reporteEquipo.alquilado,reporteEquipo.totalHoras,fechaDateStruct,
                reporteEquipo.observaciones,reporteEquipo.precio,reporteEquipo.usuario));
           }


        } else {
          console.log("ERROR AL OBTENER REPORTE EQUIPO");
          //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
        }
      });

  }

  consultarReporteEquipo(){

    this.restService.
    consultarReporteEquipo()
      .subscribe(data => {
        if (data.success === true) {
          this.exportar = [];
          this.reporteEquipos = [];

          console.log("REPORTE EQUIPO ENCONTRADO",data);

            var arrayReporteEquipo= [];
            arrayReporteEquipo = data.reporteEquipos;

            this.totalRec = arrayReporteEquipo.length;

            for (let reporteEquipo of arrayReporteEquipo) {
              console.log("REPORTE EQUIPO ENCONTRADO",reporteEquipo);

              var equipo = [reporteEquipo.equipo.idEQUIPO,reporteEquipo.equipo.nomEQUIPO,reporteEquipo.equipo.precio ]
              var propietario = [reporteEquipo.propietario.idPROPIETARIO,reporteEquipo.propietario.nomPROPIETARIO ]
              var obra = [reporteEquipo.obra.idOBRA,reporteEquipo.obra.nomOBRA ]

              var fecha = new Date(reporteEquipo.fecha);
              console.log('AQUI FECHA',fecha);

              var fechaDateStruct: NgbDateStruct = { day: fecha.getUTCDate(), month: fecha.getUTCMonth() + 1, year: fecha.getUTCFullYear()};

              var registroExportar={

                NUMERO_RECIBO :reporteEquipo.idREPORTE_EQUIPO,
                FECHA: fecha,
                EQUIPO : equipo[1],
                PRECIO_HORA : equipo[2],
                PRECIO_TOTAL : reporteEquipo.precio,
                PROPIETARIO : propietario[1],
                OBRA: obra[1],
                TOTAL_HORAS: reporteEquipo.totalHoras,
                PROPIO: reporteEquipo.propio==1?'SI':'NO',
                ALQUILADO: reporteEquipo.alquilado==1?'SI':'NO',
                USUARIO : reporteEquipo.usuario
                
              }

              this.exportar.push(registroExportar);

              this.reporteEquipos.push(new ReporteEquipos(reporteEquipo.idREPORTE_EQUIPO,
                equipo,obra,propietario,reporteEquipo.propio,reporteEquipo.alquilado,reporteEquipo.totalHoras,fechaDateStruct,
                reporteEquipo.observaciones,reporteEquipo.precio,reporteEquipo.usuario));
           }


        } else {
          console.log("ERROR AL OBTENER REPORTE EQUIPO");
          //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
        }
      });

  }

  consultarEquipos(){

    this.restService.
    consultarEquipos()
      .subscribe(data => {
        if (data.success === true) {

          this.equipos = data.equipos;

        } else {
          console.log("ERROR AL OBTENER EQUIPOS");
          //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
        }
      });

  }

  consultarActividades(){

    this.restService.
    consultarActividades()
      .subscribe(data => {
        if (data.success === true) {

          this.actividades = data.actividades;

        } else {
          console.log("ERROR AL OBTENER ACTIVIDADES");
          //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
        }
      });

  }


  consultarPropietarios(){

    this.restService.
    consultarPropietarios()
      .subscribe(data => {
        if (data.success === true) {

          this.propietarios = data.propietarios;

        } else {
          console.log("ERROR AL OBTENER PROPIETARIOES");
          //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
        }
      });

  }

  consultarObras(){

    this.restService.
    consultarObras()
      .subscribe(data => {
        if (data.success === true) {

          this.obras = data.obras;

        } else {
          console.log("ERROR AL OBTENER OBRAS");
          //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
        }
      });

  }

 

  ngOnInit() {
    this.formReporteEquipo = new FormGroup({
      'idREPORTE_EQUIPO': new FormControl({value:null,disabled: true}),
      'equipo': new FormControl('null', Validators.compose([Validators.required, Validators.pattern('.*[^ ].*')])),
      'propietario': new FormControl('null', Validators.compose([Validators.required, Validators.pattern('.*[^ ].*')])),
      'obra': new FormControl('null', Validators.compose([Validators.required, Validators.pattern('.*[^ ].*')])),
      'totalHoras': new FormControl({value:0,disabled: true}, Validators.compose([Validators.required, Validators.pattern('.*[^ ].*')])),
      'fecha': new FormControl(null, Validators.required),
      'observaciones': new FormControl(null, Validators.compose([Validators.required, Validators.pattern('.*[^ ].*')]))
		}); 
    
  }

  validarCampos(){

    if(!this.formReporteEquipo.controls['equipo'].valid){
      this.equipoValido=false;
    };

    if(!this.formReporteEquipo.controls['obra'].valid){
      this.obraValido=false;
    };

    if(!this.formReporteEquipo.controls['propietario'].valid){
      this.propietarioValido=false;
    };


    if(!this.formReporteEquipo.controls['fecha'].valid){
      this.fechaValido=false;
    };

    if(!this.formReporteEquipo.controls['observaciones'].valid){
      this.observacionesValido=false;
    };


    if(!this.formReporteEquipo.controls['totalHoras'].valid){
      this.totalHorasValido=false;
    };


  }

  // This method associate to New Button.
  onNew() {
    // Initiate new control-equipos.
    this.guardo = true;
    this.regModel = new ReporteEquipos();
    this.formReporteEquipo.reset();
    this.limpiarValidaciones();
    this.detReporteEquipos = [];
    // Change submitType to 'Save'.
    this.submitType = 'Save';
    // display control-equipos entry section.
    this.showNew = true;
    this.cdRef.detectChanges();
  }

  limpiarValidaciones(){

    this.equipoValido=true;
    this.obraValido=true;
    this.propietarioValido=true;
    this.fechaValido=true;
    this.totalHorasValido=true;
    this.observacionesValido=true;
  }

  filtrar(){
    var filtros = {} 
    var filtrar = false;
    if (this.fechaDesdeFiltro!=null){
      filtros['fechaDesde'] = new Date(this.fechaDesdeFiltro.year+'-'+this.fechaDesdeFiltro.month+'-'+this.fechaDesdeFiltro.day);
      filtrar = true;
    }

    if (this.fechaDesdeFiltro!=null){
      filtros['fechaHasta'] = new Date(this.fechaHastaFiltro.year+'-'+this.fechaHastaFiltro.month+'-'+this.fechaHastaFiltro.day);
      filtrar = true;
    }
    if (this.numeroReciboFiltro!=null&&this.numeroReciboFiltro.length>0){
      filtros['idREPORTE_EQUIPO'] = this.numeroReciboFiltro;
      filtrar = true
    }

    if (filtrar==true){
      this.consultarReporteEquipoFiltros(filtros);
    }else{
      this.consultarReporteEquipo();
    }
    

    
  }

  // This method associate to Save Button.
  onSave() {

    this.validarCampos();
    

    if(this.formReporteEquipo.valid && this.regModel.totalHoras>0){
    if (this.submitType === 'Save') {
      // Push control-equipos model object into control-equipos list.
      //this.reporteEquipos.push(this.regModel);

     var datos = this.obtenerArregloDatosInsert();

      this.restService.
       insertarReporteEquipo(datos)
        .subscribe(data => {
          if (data.success === true) {

          

                var count =0;

                for (let detReporteEquipo of this.detReporteEquipos){

                  var datos = this.obtenerArregloDatosInsertDet(detReporteEquipo);
                  datos['idREPORTE_EQUIPO'] = data.reporteEquipos.idREPORTE_EQUIPO;
     
                this.restService.
                insertarDetReporteEquipo(datos)
                 .subscribe(data1 => {
                   if (data1.success === true) {
                      
                      count++;
         
                      if (count == this.detReporteEquipos.length){

                        this.consultarReporteEquipo();

                      }
                   
                   } else {
                     console.log("ERROR AL INSERTAR DET REPORTE EQUIPO");
                     //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
                   }
                 });
                }
               
                this.regModel.idREPORTE_EQUIPO = data.reporteEquipos.idREPORTE_EQUIPO;
           
          
          } else {
            console.log("ERROR AL INSERTAR REPORTE EQUIPO");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });

  


    } else {
      // Update the existing properties values based on model.

      var datos = this.obtenerArregloDatosInsert();

      this.restService.
       editarReporteEquipo(this.regModel.idREPORTE_EQUIPO,datos)
        .subscribe(data => {
          if (data.success === true) {

            this.restService.
            eliminarDetReporteEquipo(this.regModel.idREPORTE_EQUIPO)
             .subscribe(data1 => {
               if (data1.success === true) {

                console.log("ELIMINA DET REPORTE EQUIPO EDITAR");

                var count =0;

                for (let detReporteEquipo of this.detReporteEquipos){

                  var datos = this.obtenerArregloDatosInsertDet(detReporteEquipo);
                  datos['idREPORTE_EQUIPO'] = this.regModel.idREPORTE_EQUIPO;
     
                this.restService.
                insertarDetReporteEquipo(datos)
                 .subscribe(data2 => {
                   if (data2.success === true) {

                    console.log("INSERTA DET REPORTE EQUIPO EDITAR");

                      count++;

         
                      if (count == this.detReporteEquipos.length){

                        this.consultarReporteEquipo();

                      }
                   
                   } else {
                     console.log("ERROR AL INSERTAR DET REPORTE EQUIPO");
                     //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
                   }
                 });
                }
               
               } else {
                 console.log("ERROR AL ELIMINAR DET REPORTE EQUIPO");
                 //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
               }
             });

          } else {
            console.log("ERROR AL INSERTAR REPORTE EQUIPO");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });


    }
    // Hide control-equipos entry section.
    //this.showNew = false;
    //this.modalClose.nativeElement.click();
    this.guardo=false;
  }
}

  obtenerArregloDatosInsert(){

    var fecha = this.regModel.fecha.year+'-'+this.regModel.fecha.month+'-'+this.regModel.fecha.day;

    this.regModel.precio = (this.regModel.totalHoras * parseInt( this.regModel.equipo[2])).toString();

    this.regModel.usuario = this.user.login;

    var datos = {
      idEQUIPO :  this.regModel.equipo[0],
      idPROPIETARIO:  this.regModel.propietario[0],
      idOBRA:  this.regModel.obra[0],
      propio : this.regModel.propio,
      alquilado : this.regModel.alquilado,
      totalHoras : this.regModel.totalHoras,
      precio: this.regModel.precio,
      fecha:fecha,
      observaciones : this.regModel.observaciones,
      usuario: this.regModel.usuario
      
    }

    return datos;

  }

  // This method associate to Edit Button.
  onEdit(index: number) {
    this.consultarDetReporteEquiposIndex(index);
    this.limpiarValidaciones();
    // Assign selected table row index.
    this.selectedRow = index;
    // Initiate new control-equipos.
    this.regModel = new ReporteEquipos();
    // Retrieve selected control-equipos from list and assign to model.
    this.regModel = Object.assign({}, this.reporteEquipos[this.selectedRow]);
    // Change submitType to Update.
    this.submitType = 'Update';
    // Display control-equipos entry section.
    this.showNew = true;

    this.detReporteEquipos = [];
  }

  // This method associate to Delete Button.
   // This method associate to Delete Button.
   onDelete(index: number) {
    // Delete the corresponding material entry from the list.


    this.restService.
    eliminarDetReporteEquipo(this.reporteEquipos[index].idREPORTE_EQUIPO)
     .subscribe(data1 => {
       if (data1.success === true) {

        this.restService.
        eliminarReporteEquipo(this.reporteEquipos[index].idREPORTE_EQUIPO)
          .subscribe(data => {
            if (data.success === true) {
              this.reporteEquipos.splice(index,1);
            } else {
              console.log("ERROR AL ELIMINAR REPORTE EQUIPO");
              //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
            }
          });
       
       } else {
         console.log("ERROR AL ELIMINAR DET REPORTE EQUIPO");
         //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
       }
     });



    console.log("AQUI CONFIRMAR ELIMINACION",index);
    
    
    
  }

  // This method associate toCancel Button.
  onCancel() {
    // Hide control-equipos entry section.
    this.showNew = false;
  }

  // This method associate to Bootstrap dropdown selection change.
  onChangeEquipo(equipo) {
    // Assign corresponding selected country to model.
    this.regModel.equipo[0] = equipo.idEQUIPO;
    this.regModel.equipo[1] = equipo.nomEQUIPO;
    this.regModel.equipo[2] = equipo.precio;
  }

  // This method associate to Bootstrap dropdown selection change.
  onChangeObra(obra) {
    // Assign corresponding selected country to model.
    this.regModel.obra[0] = obra.idOBRA;
    this.regModel.obra[1] = obra.nomOBRA;
  }

 

  onChangePropietario(propietario) {
    // Assign corresponding selected country to model.
    this.regModel.propietario[0] = propietario.idPROPIETARIO;
    this.regModel.propietario[1] = propietario.nomPROPIETARIO;
  }

  onChangeActividad(actividad) {
    // Assign corresponding selected country to model.
    this.detRegModel.actividad[0] = actividad.idACTIVIDAD;
    this.detRegModel.actividad[1] = actividad.nomACTIVIDAD;
  }

 

  checkPropio(checked){

    if(checked){
      this.regModel.propio=1;
      this.regModel.alquilado=0;
    }else{
      this.regModel.propio=0;
      this.regModel.alquilado=1;
    }
  }

  checkAlquilado(checked){

    if(checked){
      this.regModel.alquilado=1;
      this.regModel.propio=0;
    }else{
      this.regModel.alquilado=0;
      this.regModel.propio=1;
    }
  }

   // This method associate to Delete Button.
   confirmDelete(index: number) {
    console.log("AQUI CONFIRMAR ELIMINACION",index);
    this.selectedIndex = index;
  }

  exportarExcel(){

    /* make the worksheet */
    var ws = XLSX.utils.json_to_sheet(this.exportar);

    /* add to workbook */
    var wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "People");

    /* write workbook (use type 'binary') */
    var wbout = XLSX.write(wb, {bookType:'xlsx', type:'binary'});

    var today = new Date();
    var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
    var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
    var name = "reporteReporteEquipo" + date + time;

    FileSaver.saveAs(new Blob([this.s2ab(wbout)],{type:"application/octet-stream"}), name+".xlsx");
  }

   s2ab(s) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
  }

   exportAsExcelFile(json: any[], excelFileName: string): void {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }

    private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
          type: "application/octet-stream"
        });
        var today = new Date();
        var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
        var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
        var name = fileName + date + time;
        FileSaver.saveAs(data, name + ".xlsx");
}





  //DETALLE REPORTE EQUIPOS
  inicializarDetReporteEquipos(){

    this.formDetReporteEquipo = new FormGroup({
      'horaDesde': new FormControl(null, Validators.required),
      'horaHasta': new FormControl(null, Validators.required),
      'actividad': new FormControl('null', Validators.compose([Validators.required, Validators.pattern('.*[^ ].*')]))
		}); 

  }

  consultarDetReporteEquipos(){
    

    this.detReporteEquipos = [];

    this.restService.
    consultarDetReporteEquipo(this.regModel.idREPORTE_EQUIPO)
      .subscribe(data => {
        if (data.success === true) {
          this.exportar = [];

          console.log("DET REPORTE EQUIPO ENCONTRADO",data);

            var arrayDetReporteEquipos = [];
            arrayDetReporteEquipos= data.detReporteEquipos;

            this.totalRecDet = arrayDetReporteEquipos.length;



            for (let detReporteEquipo of arrayDetReporteEquipos) {
              console.log("DET REPORTE EQUIPO ENCONTRADO",detReporteEquipo);

              var horaDesde = new Date(detReporteEquipo.horaDesde);
              var horaHasta = new Date(detReporteEquipo.horaHasta);

              var horaDesdeRegModel = {hour:horaDesde.getHours(), minute:horaDesde.getMinutes()};
              var horaHastaRegModel = {hour:horaHasta.getHours(), minute:horaHasta.getMinutes()};
              
              var actividad = [detReporteEquipo.actividad.idACTIVIDAD,detReporteEquipo.actividad.nomACTIVIDAD ]

              this.detReporteEquipos.push(new DetReporteEquipos(detReporteEquipo.idDET_REPORTE_EQUIPO,this.regModel.idREPORTE_EQUIPO,
                detReporteEquipo.horas,actividad,horaDesdeRegModel,horaHastaRegModel));
           }


        } else {
          console.log("ERROR AL OBTENER DET REPORTE EQUIPOS");
          //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
        }
      });

  }

  consultarDetReporteEquiposIndex(index){
    

    this.detReporteEquipos = [];

    this.restService.
    consultarDetReporteEquipo(this.reporteEquipos[index].idREPORTE_EQUIPO)
      .subscribe(data => {
        if (data.success === true) {
          this.exportar = [];

          console.log("DET REPORTE EQUIPO ENCONTRADO",data);

            var arrayDetReporteEquipos = [];
            arrayDetReporteEquipos= data.detReporteEquipos;

            this.totalRecDet = arrayDetReporteEquipos.length;



            for (let detReporteEquipo of arrayDetReporteEquipos) {
              console.log("DET REPORTE EQUIPO ENCONTRADO",detReporteEquipo);

              var horaDesde = new Date(detReporteEquipo.horaDesde);
              var horaHasta = new Date(detReporteEquipo.horaHasta);

              var horaDesdeRegModel = {hour:horaDesde.getHours(), minute:horaDesde.getMinutes()};
              var horaHastaRegModel = {hour:horaHasta.getHours(), minute:horaHasta.getMinutes()};
              var actividad = [detReporteEquipo.actividad.idACTIVIDAD,detReporteEquipo.actividad.nomACTIVIDAD ]

              this.detReporteEquipos.push(new DetReporteEquipos(detReporteEquipo.idDET_REPORTE_EQUIPO,this.regModel.idREPORTE_EQUIPO,
                detReporteEquipo.horas,actividad,horaDesdeRegModel,horaHastaRegModel));
           }


        } else {
          console.log("ERROR AL OBTENER DET REPORTE EQUIPOS");
          //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
        }
      });

  }

  

  validarCamposDet(){

    if(!this.formDetReporteEquipo.controls['horaDesde'].valid){
      this.horaDesdeValido=false;
    };

    if(!this.formDetReporteEquipo.controls['horaHasta'].valid){
      this.horaHastaValido=false;
    };

    if(!this.formDetReporteEquipo.controls['actividad'].valid){
      this.actividadValido=false;
    };
    


  }


   // This method associate to New Button.
   onVerDet() {
    if (this.regModel.idREPORTE_EQUIPO !=null){
    this.consultarDetReporteEquipos();
    }
    this.inicializarDetReporteEquipos();
    this.limpiarValidacionesDet();
    // display control-equipos entry section.
    this.showVerDet = true;
    this.cdRef.detectChanges();
  }


  // This method associate to New Button.
  onNewDet() {

    this.inicializarDetReporteEquipos();
    // Initiate new control-equipos.
    this.detRegModel = new DetReporteEquipos();
    this.formDetReporteEquipo.reset();
    this.limpiarValidacionesDet();
    // Change submitType to 'Save'.
    this.submitTypeDet = 'Save';
    // display control-equipos entry section.
    this.showNewDet = true;
    this.cdRef.detectChanges();
  }

  limpiarValidacionesDet(){

    this.horaDesdeValido=true;
    this.horaHastaValido=true;
    this.actividadValido=true;
  }


  onSaveDet() {

    this.validarCamposDet();
    

    if(this.formDetReporteEquipo.valid){
    if (this.submitTypeDet === 'Save') {
     
      this.detReporteEquipos.push(new DetReporteEquipos(this.detRegModel.idDET_REPORTE_EQUIPO,this.regModel.idREPORTE_EQUIPO,
      this.calcularHorasRegistroDetalle(),  this.detRegModel.actividad,  this.detRegModel.horaDesde,this.detRegModel.horaHasta));
     

    } else {
      // Update the existing properties values based on model.

      this.detReporteEquipos[this.selectedRowDet].horaDesde = this.detRegModel.horaDesde;
      this.detReporteEquipos[this.selectedRowDet].horaHasta = this.detRegModel.horaHasta;
      this.detReporteEquipos[this.selectedRowDet].actividad = this.detRegModel.actividad;


    }
    // Hide control-equipos entry section.
    this.totalRecDet = this.reporteEquipos.length;
    this.showNewDet = false;
    this.calcularHorasReporteEquipo();
    this.modalCloseDet.nativeElement.click();

  }
}

  obtenerArregloDatosInsertDet(detReporteEquipo){
    
    var horaDesde = new Date();
    horaDesde.setHours(detReporteEquipo.horaDesde.hour);
    horaDesde.setMinutes(detReporteEquipo.horaDesde.minute);
    var horaDesdeFormat = this.datePipe.transform(horaDesde, 'yyyy-MM-dd HH:mm:ss');

    var horaHasta = new Date();
    horaHasta.setHours(detReporteEquipo.horaHasta.hour);
    horaHasta.setMinutes(detReporteEquipo.horaHasta.minute);
    var horaHastaFormat = this.datePipe.transform(horaHasta, 'yyyy-MM-dd HH:mm:ss');

    var datos = {
      horaDesde:  horaDesdeFormat,
      horaHasta:  horaHastaFormat,
      idActividad:  detReporteEquipo.actividad[0],
      horas: detReporteEquipo.horas
      
    }
    
    return datos;

  }

  // This method associate to Edit Button.
  onEditDet(index: number) {
    // Assign selected table row index.
    this.selectedRowDet = index;
    // Initiate new control-equipos.
    this.detRegModel = new DetReporteEquipos();
    // Retrieve selected control-equipos from list and assign to model.
    this.detRegModel = Object.assign({}, this.detReporteEquipos[this.selectedRowDet]);
    // Change submitType to Update.
    this.submitTypeDet = 'Update';
    // Display control-equipos entry section.
    this.showNewDet = true;
  }

  // This method associate to Delete Button.
   // This method associate to Delete Button.onSaveDet
   onDeleteDet(index: number) {
    
    this.detReporteEquipos.splice(index,1);
    this.totalRecDet = this.detReporteEquipos.length;
       
  }

  // This method associate toCancel Button.
  onCancelDet() {
    // Hide control-equipos entry section.
    this.showNewDet = false;
  }


  // This method associate to Delete Button.
  confirmDeleteDet(index: number) {
    console.log("AQUI CONFIRMAR ELIMINACION DET",index);
    this.selectedIndexDet = index;
  }

  calcularHorasRegistroDetalle(){

    var horaDesde = new Date();
    horaDesde.setHours(this.detRegModel.horaDesde.hour);
    horaDesde.setMinutes(this.detRegModel.horaDesde.minute);


    var horaHasta = new Date();
    horaHasta.setHours(this.detRegModel.horaHasta.hour);
    horaHasta.setMinutes(this.detRegModel.horaHasta.minute);

    var hours = Math.abs(horaHasta.getTime() - horaDesde.getTime()) / 36e5;

    return Math.round(hours);

  }

  calcularHorasReporteEquipo(){

    var totalHoras = 0;
    for (let detReporteEquipo of this.detReporteEquipos){

      totalHoras += detReporteEquipo.horas;

    }

    this.regModel.totalHoras = totalHoras;

  }

  generarRecibo(){

    this.detReporteEquipos = [];
    var promises = [];

    this.restService.
    consultarDetReporteEquipo(this.regModel.idREPORTE_EQUIPO)
      .subscribe(data => {
        if (data.success === true) {
          this.exportar = [];

          console.log("DET REPORTE EQUIPO ENCONTRADO",data);

            var arrayDetReporteEquipos = [];
            arrayDetReporteEquipos= data.detReporteEquipos;

            this.totalRecDet = arrayDetReporteEquipos.length;



            for (let detReporteEquipo of arrayDetReporteEquipos) {
              console.log("DET REPORTE EQUIPO ENCONTRADO",detReporteEquipo);

              var horaDesde = new Date(detReporteEquipo.horaDesde);
              var horaHasta = new Date(detReporteEquipo.horaHasta);

              var horaDesdeRegModel = {hour:horaDesde.getHours(), minute:horaDesde.getMinutes()};
              var horaHastaRegModel = {hour:horaHasta.getHours(), minute:horaHasta.getMinutes()};
              
              var actividad = [detReporteEquipo.actividad.idACTIVIDAD,detReporteEquipo.actividad.nomACTIVIDAD ]

              promises.push( 

              new Promise((resolve, reject) => {
                console.log('Initial');
                this.detReporteEquipos.push(new DetReporteEquipos(detReporteEquipo.idDET_REPORTE_EQUIPO,this.regModel.idREPORTE_EQUIPO,
                  detReporteEquipo.horas,actividad,horaDesdeRegModel,horaHastaRegModel));
                resolve();
            })


              )
           }

           Promise.all(promises).then((values) => { 
            var fecha = this.regModel.fecha.day+'/'+this.regModel.fecha.month+'/'+this.regModel.fecha.year;

            var arrayExport = [];
            
            arrayExport[0] = "<html><head></head><body><print>";
            arrayExport[1] = "       COBICIVIL S.A.S";
            arrayExport[2] = "www.cobicivil.com - PBX: 5586151";
            arrayExport[3] = "--------------------------------";
            arrayExport[4] = "REPORTE EQUIPO";
            arrayExport[5] = "Recibo No.        "+this.regModel.idREPORTE_EQUIPO;
            arrayExport[6] = "Fecha:            "+fecha;
            arrayExport[7] = "Equipo:      "+this.regModel.equipo[1];
            arrayExport[8] = "Obra:        "+this.regModel.obra[1];
            arrayExport[9] = "Propietario: "+this.regModel.propietario[1];
            arrayExport[10]  = "Total Horas:     "+this.regModel.totalHoras;
            arrayExport[11] = "--------------------------------";
            arrayExport[12] = "De:     A:     Actividad:"
        
        
            for (let detalle of this.detReporteEquipos){
        
            
              var horaDesde = new Date();
          
              horaDesde.setHours(detalle['horaDesde'].hour);
              horaDesde.setMinutes(detalle['horaDesde'].minute);
          
              var horaHasta = new Date();
              horaHasta.setHours(detalle['horaHasta'].hour);
              horaHasta.setMinutes(detalle['horaHasta'].minute);
          
              var horaDesdeRegModel = {hour:horaDesde.getHours(), minute:horaDesde.getMinutes()};
              var horaHastaRegModel = {hour:horaHasta.getHours(), minute:horaHasta.getMinutes()};
              
              arrayExport[arrayExport.length] = horaDesdeRegModel.hour.toString().length>1?horaDesdeRegModel.hour:'0'+horaDesdeRegModel.hour+":"+ (horaDesdeRegModel.minute.toString().length>1?horaDesdeRegModel.minute:'0'+horaDesdeRegModel.minute)+"   "+(horaHastaRegModel.hour.toString().length>1?horaHastaRegModel.hour:'0'+horaHastaRegModel.hour)+":"+(horaHastaRegModel.minute.toString().length>1?horaHastaRegModel.minute:'0'+horaHastaRegModel.minute)+"  "+detalle['actividad'][1];
        
            }
            arrayExport[arrayExport.length] = "--------------------------------";
            this.regModel.propio==1? arrayExport[arrayExport.length] =     "Propio:     SI":arrayExport[arrayExport.length] = "Propio:     NO";
            this.regModel.alquilado==1? arrayExport[arrayExport.length] =  "Alquilado:  SI":arrayExport[arrayExport.length] = "Alquilado:  NO";
            arrayExport[arrayExport.length] = "Observaciones:  ";
            arrayExport[arrayExport.length] = this.regModel.observaciones;
         
            arrayExport[arrayExport.length] = "--------------------------------";
            arrayExport[arrayExport.length] = "Usuario:    "+this.regModel.usuario;

            arrayExport[arrayExport.length] = "Precio Hora:    $"+this.regModel.equipo[2]+".00";
            arrayExport[arrayExport.length] = "Precio Total:   $"+this.regModel.precio+".00";
            arrayExport[arrayExport.length] = "Firma responsable:";
            arrayExport[arrayExport.length] = "<br>";
            arrayExport[arrayExport.length] = "<br>";
            arrayExport[arrayExport.length] = "--------------------------------";
            arrayExport[arrayExport.length] = new Date().toLocaleString();
            arrayExport[arrayExport.length] = "<br>";
            arrayExport[arrayExport.length] = "<br>";
            arrayExport[arrayExport.length] = "</print></body></html>";
        
          
            var data = JSON.stringify(arrayExport);
        
            var res = data.replace(/['"]+/g, '');
        
            res = res.replace(/,/g, "\n");
        
            res = res.replace("[", '');
        
            res = res.replace("]", '');
        
            
            var blob = new Blob([res], {type: "text/html;charset=utf-8"});
            FileSaver.saveAs(blob, "Equipos_Recibo_"+this.regModel.idREPORTE_EQUIPO);
          });


        } else {
          console.log("ERROR AL OBTENER DET REPORTE EQUIPOS");
          //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
        }
      });
    
  


  }



}
