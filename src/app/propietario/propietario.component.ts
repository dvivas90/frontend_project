import { Component, OnInit,ViewChild, ElementRef} from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import FileSaver from 'file-saver';
import { RestService} from '../providers/rest.service';
import * as XLSX from 'xlsx';

class Propietario {
  constructor(
    public idPROPIETARIO: string = '',
    public nomPROPIETARIO: string = '',
    public desPROPIETARIO: string = ''
  ) {}
}

@Component({
  selector: 'app-propietario',
  templateUrl: './propietario.component.html',
  styleUrls: ['./propietario.component.css']
})
export class PropietarioComponent implements OnInit {
  formPropietario: FormGroup;
  @ViewChild('modalClose') modalClose:ElementRef;
  
  // It maintains list of propietarios
  propietarios: Propietario[] = [];
  // It maintains propietario Model
  regModel: Propietario;
  // It maintains propietario form display status. By default it will be false.
  showNew: Boolean = false;
  // It will be either 'Save' or 'Update' based on operation.
  submitType: string = 'Save';
  // It maintains table row index based on selection.
  selectedRow: number;
  // It maintains Array of countries.
  totalRec : number;
  page: number = 1;

  selectedIndex : number;

  public filtroNombre: String = null;

  constructor(private restService:RestService) {
    
    this.consultarPropietarios();

  }

  consultarPropietarios(){
    // Add default propietario data.
    this.propietarios = [];

    this.restService.
      consultarPropietarios()
        .subscribe(data => {
          if (data.success === true) {

            var arrayPropietarios = [];
            arrayPropietarios = data.propietarios;

            this.totalRec = arrayPropietarios.length;

            for (let propietario of arrayPropietarios) {
              console.log("PROPIETARIO ENCONTRADA",propietario);
              this.propietarios.push(new Propietario(propietario.idPROPIETARIO,propietario.nomPROPIETARIO, propietario.desPROPIETARIO));
           }
          
  
          } else {
            console.log("ERROR AL OBTENER PROPIETARIOS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });
  }

  ngOnInit() {
    this.formPropietario = new FormGroup({
      'idPROPIETARIO': new FormControl({value:null,disabled: true}),
      'nomPROPIETARIO': new FormControl(null, Validators.compose([Validators.required,Validators.pattern(".*[^ ].*")])), 
      'desPROPIETARIO': new FormControl(null, Validators.compose([Validators.required,Validators.pattern(".*[^ ].*")]))
		});
    
  }

  // This method associate to New Button.
  onNew() {
    // Initiate new propietario.
    this.regModel = new Propietario();
    this.formPropietario.reset();
    // Change submitType to 'Save'.
    this.submitType = 'Save';
    // display propietario entry section.
    this.showNew = true;
  }

  filtrar(){
    var date = new Date();

    if (this.filtroNombre!=null && this.filtroNombre!=''){

      var filtros = { 
          nomPROPIETARIO:this.filtroNombre

      }

      this.restService.
       consultarPropietariosFiltros(filtros)
        .subscribe(data => {
          if (data.success === true) {

            this.propietarios = data.propietarios;

            this.totalRec = this.propietarios.length;
          
          } else {
            console.log("ERROR AL CONSULTAR PROPIETARIOS FILTROS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });

    }else{
      this.consultarPropietarios();
    }

    
  
  }

  // This method associate to Save Button.
  onSave() {
    
    if(this.formPropietario.valid){

    if (this.submitType === 'Save') {
      // Push propietario model object into propietario list.

      var propietario = {
        nomPROPIETARIO:this.regModel.nomPROPIETARIO.trim(),
        desPROPIETARIO:this.regModel.desPROPIETARIO.trim()
      }
      this.restService.
       insertarPropietario(propietario)
        .subscribe(data => {
          if (data.success === true) {

            this.propietarios.push(data.propietario);
          
          } else {
            console.log("ERROR AL INSERTAR PROPIETARIOS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });


    } else {
      // Update the existing properties values based on model.

      this.restService.
       editarPropietario(this.regModel.idPROPIETARIO,this.regModel)
        .subscribe(data => {
          if (data.success === true) {
          this.propietarios[this.selectedRow].idPROPIETARIO = this.regModel.idPROPIETARIO;
          this.propietarios[this.selectedRow].nomPROPIETARIO = this.regModel.nomPROPIETARIO;
          this.propietarios[this.selectedRow].desPROPIETARIO = this.regModel.desPROPIETARIO;
          } else {
            console.log("ERROR AL EDITAR PROPIETARIOS");
          }
        });
    }
    // Hide propietario entry section.
    this.showNew = false;
    this.modalClose.nativeElement.click();
    }
  }

  // This method associate to Edit Button.
  onEdit(index: number) {
    // Assign selected table row index.
    this.selectedRow = index;
    // Initiate new propietario.
    this.regModel = new Propietario();

    console.log('PROPIETARIO EDITAR',this.propietarios[this.selectedRow])
    // Retrieve selected propietario from list and assign to model.
    this.regModel = Object.assign({}, this.propietarios[this.selectedRow]);
    // Change submitType to Update.
    this.submitType = 'Update';
    // Display propietario entry section.
    this.showNew = true;
  }

  // This method associate to Delete Button.
  confirmDelete(index: number) {
    console.log("AQUI CONFIRMAR ELIMINACION",index);
    this.selectedIndex = index;
  }

  // This method associate to Delete Button.
  onDelete(index: number) {
    // Delete the corresponding propietario entry from the list.
    console.log("AQUI CONFIRMAR ELIMINACION",index);
    this.restService.
      eliminarPropietario(this.propietarios[index].idPROPIETARIO)
        .subscribe(data => {
          if (data.success === true) {
            this.propietarios.splice(index,1);
          } else {
            console.log("ERROR AL ELIMINAR PROPIETARIOS");
            //this.alertService.alertaInformativa('OPS', 'ERROR_GENERICO');
          }
        });
    
    
  }

  // This method associate toCancel Button.
  onCancel() {
    // Hide propietario entry section.
    this.showNew = false;
  }

 

  exportarExcel(){

    /* make the worksheet */
    var ws = XLSX.utils.json_to_sheet(this.propietarios);

    /* add to workbook */
    var wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "People");

    /* write workbook (use type 'binary') */
    var wbout = XLSX.write(wb, {bookType:'xlsx', type:'binary'});

    var today = new Date();
    var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
    var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
    var name = "reportePropietario" + date + time;

    FileSaver.saveAs(new Blob([this.s2ab(wbout)],{type:"application/octet-stream"}), name+".xlsx");
  }

   s2ab(s) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
  }

   exportAsExcelFile(json: any[], excelFileName: string): void {
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }

    private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
          type: "application/octet-stream"
        });
        var today = new Date();
        var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
        var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
        var name = fileName + date + time;
        FileSaver.saveAs(data, name + ".xlsx");
}




}
