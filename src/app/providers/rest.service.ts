import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { ConfigurationProvider } from './configuration';

@Injectable()
export class RestService {

  constructor(public http: Http, public confProvider: ConfigurationProvider) {
    console.log('Hello RestServiceProvider Provider');
  }

  consultarObrasFiltros(filtros) {
    return Observable.create(observer => {
      var postData = JSON.stringify({
        filtros:filtros
      });

      this.http.post(this.confProvider.config.consultarObrasFiltros, postData).subscribe(data => {
        observer.next(data.json());
      }, err => {
        console.log(err);
        observer.error(err);
      });
    });
  }



  consultarObras() {
    return Observable.create(observer => {
      var postData = JSON.stringify({
      });

      this.http.post(this.confProvider.config.consultarObras, postData).subscribe(data => {
        observer.next(data.json());
      }, err => {
        console.log(err);
        observer.error(err);
      });
    });
  }

  insertarObra(datosObra) {

    console.log("datos insertar obra",datosObra);

    return Observable.create(observer => {
      var postData = JSON.stringify({
        datosObra: datosObra
      });
      this.http.post(this.confProvider.config.insertarObra, postData).subscribe(data => {
        observer.next(data.json());
      }, err => {
        console.log(err);
        observer.error(err);
      });
    });
  }

  editarObra(idOBRA,campos) {

    console.log("datos editar obra",campos);

    return Observable.create(observer => {
      var postData = JSON.stringify({
        idOBRA: idOBRA,
        campos: campos
      });
      this.http.post(this.confProvider.config.editarObra, postData).subscribe(data => {
        observer.next(data.json());
      }, err => {
        console.log(err);
        observer.error(err);
      });
    });
  }

  eliminarObra(idOBRA) {
    return Observable.create(observer => {
      var postData = JSON.stringify({
        idOBRA: idOBRA
      });
      this.http.post(this.confProvider.config.eliminarObra, postData).subscribe(data => {
        observer.next(data.json());
      }, err => {
        console.log(err);
        observer.error(err);
      });
    });
  }

//CLIENTES

consultarClientesFiltros(filtros) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      filtros:filtros
    });

    this.http.post(this.confProvider.config.consultarClientesFiltros, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}



consultarClientes() {
  return Observable.create(observer => {
    var postData = JSON.stringify({
    });

    this.http.post(this.confProvider.config.consultarClientes, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

insertarCliente(datosCliente) {

  console.log("datos insertar cliente",datosCliente);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      datosCliente: datosCliente
    });
    this.http.post(this.confProvider.config.insertarCliente, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

editarCliente(idCLIENTE,campos) {

  console.log("datos editar cliente",campos);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      idCLIENTE: idCLIENTE,
      campos: campos
    });
    this.http.post(this.confProvider.config.editarCliente, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

eliminarCliente(idCLIENTE) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      idCLIENTE: idCLIENTE
    });
    this.http.post(this.confProvider.config.eliminarCliente, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

//MATERIAL

consultarMaterialesFiltros(filtros) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      filtros:filtros
    });

    this.http.post(this.confProvider.config.consultarMaterialesFiltros, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}



consultarMateriales() {
  return Observable.create(observer => {
    var postData = JSON.stringify({
    });

    this.http.post(this.confProvider.config.consultarMateriales, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

insertarMaterial(datosMaterial) {

  console.log("datos insertar equipo",datosMaterial);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      datosMaterial: datosMaterial
    });
    this.http.post(this.confProvider.config.insertarMaterial, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

editarMaterial(idMATERIAL,campos) {

  console.log("datos editar material",campos);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      idMATERIAL: idMATERIAL,
      campos: campos
    });
    this.http.post(this.confProvider.config.editarMaterial, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

eliminarMaterial(idMATERIAL) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      idMATERIAL: idMATERIAL
    });
    this.http.post(this.confProvider.config.eliminarMaterial, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

//PROVEEDOR

consultarProveedoresFiltros(filtros) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      filtros:filtros
    });

    this.http.post(this.confProvider.config.consultarProveedoresFiltros, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}



consultarProveedores() {
  return Observable.create(observer => {
    var postData = JSON.stringify({
    });

    this.http.post(this.confProvider.config.consultarProveedores, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

insertarProveedor(datosProveedor) {

  console.log("datos insertar proveedor",datosProveedor);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      datosProveedor: datosProveedor
    });
    this.http.post(this.confProvider.config.insertarProveedor, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

editarProveedor(idPROVEEDOR,campos) {

  console.log("datos editar proveedor",campos);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      idPROVEEDOR: idPROVEEDOR,
      campos: campos
    });
    this.http.post(this.confProvider.config.editarProveedor, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

eliminarProveedor(idPROVEEDOR) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      idPROVEEDOR: idPROVEEDOR
    });
    this.http.post(this.confProvider.config.eliminarProveedor, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

//EQUIPO

consultarEquiposFiltros(filtros) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      filtros:filtros
    });

    this.http.post(this.confProvider.config.consultarEquiposFiltros, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}



consultarEquipos() {
  return Observable.create(observer => {
    var postData = JSON.stringify({
    });

    this.http.post(this.confProvider.config.consultarEquipos, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

insertarEquipo(datosEquipo) {

  console.log("datos insertar equipo",datosEquipo);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      datosEquipo: datosEquipo
    });
    this.http.post(this.confProvider.config.insertarEquipo, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

editarEquipo(idEQUIPO,campos) {

  console.log("datos editar equipo",campos);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      idEQUIPO: idEQUIPO,
      campos: campos
    });
    this.http.post(this.confProvider.config.editarEquipo, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

eliminarEquipo(idEQUIPO) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      idEQUIPO: idEQUIPO
    });
    this.http.post(this.confProvider.config.eliminarEquipo, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

//PROPIETARIO

consultarPropietariosFiltros(filtros) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      filtros:filtros
    });

    this.http.post(this.confProvider.config.consultarPropietarioFiltros, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}



consultarPropietarios() {
  return Observable.create(observer => {
    var postData = JSON.stringify({
    });

    this.http.post(this.confProvider.config.consultarPropietarios, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

insertarPropietario(datosPropietario) {

  console.log("datos insertar propietario",datosPropietario);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      datosPropietario: datosPropietario
    });
    this.http.post(this.confProvider.config.insertarPropietario, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

editarPropietario(idPROPIETARIO,campos) {

  console.log("datos editar propietario",campos);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      idPROPIETARIO: idPROPIETARIO,
      campos: campos
    });
    this.http.post(this.confProvider.config.editarPropietario, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

eliminarPropietario(idPROPIETARIO) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      idPROPIETARIO: idPROPIETARIO
    });
    this.http.post(this.confProvider.config.eliminarPropietario, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

//CONTROLMATERIALES

consultarControlMaterialesFiltros(filtros) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      filtros:filtros
    });

    this.http.post(this.confProvider.config.consultarControlMaterialesFiltros, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}



consultarControlMateriales() {
  return Observable.create(observer => {
    var postData = JSON.stringify({
    });

    this.http.post(this.confProvider.config.consultarControlMateriales, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

insertarControlMateriales(datosControlMateriales) {

  console.log("datos insertar control materiales",datosControlMateriales);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      datosControlMateriales: datosControlMateriales
    });
    this.http.post(this.confProvider.config.insertarControlMateriales, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

editarControlMateriales(idCONTROL_MATERIALES,campos) {

  console.log("datos editar control materiales",campos);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      idCONTROL_MATERIALES: idCONTROL_MATERIALES,
      campos: campos
    });
    this.http.post(this.confProvider.config.editarControlMateriales, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

eliminarControlMateriales(idCONTROL_MATERIALES) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      idCONTROL_MATERIALES: idCONTROL_MATERIALES
    });
    this.http.post(this.confProvider.config.eliminarControlMateriales, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

//DETALLE REPORTE EQUIPO

consultarDetReporteEquipo(idREPORTE_EQUIPO) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      idREPORTE_EQUIPO:idREPORTE_EQUIPO
    });

    this.http.post(this.confProvider.config.consultarDetReporteEquipos, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}





insertarDetReporteEquipo(datosDetReporteEquipo) {

  console.log("datos insertar detalle reporte equipo",datosDetReporteEquipo);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      datosDetReporteEquipo: datosDetReporteEquipo
    });
    this.http.post(this.confProvider.config.insertarDetReporteEquipo, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

editarDetReporteEquipo(idDET_REPORTE_EQUIPO,campos) {

  console.log("datos editar detalle reporte equipo",campos);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      idDET_REPORTE_EQUIPO: idDET_REPORTE_EQUIPO,
      campos: campos
    });
    this.http.post(this.confProvider.config.editarDetReporteEquipo, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

eliminarDetReporteEquipo(idREPORTE_EQUIPO) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      idREPORTE_EQUIPO: idREPORTE_EQUIPO
    });
    this.http.post(this.confProvider.config.eliminarDetReporteEquipo, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

//REPORTE EQUIPO

consultarReporteEquipoFiltros(filtros) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      filtros:filtros
    });

    this.http.post(this.confProvider.config.consultarReporteEquipoFiltros, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}



consultarReporteEquipo() {
  return Observable.create(observer => {
    var postData = JSON.stringify({
    });

    this.http.post(this.confProvider.config.consultarReporteEquipo, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

insertarReporteEquipo(datosReporteEquipo) {

  console.log("datos insertar propietario",datosReporteEquipo);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      datosReporteEquipo: datosReporteEquipo
    });
    this.http.post(this.confProvider.config.insertarReporteEquipo, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

editarReporteEquipo(idREPORTE_EQUIPO,campos) {

  console.log("datos editar propietario",campos);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      idREPORTE_EQUIPO: idREPORTE_EQUIPO,
      campos: campos
    });
    this.http.post(this.confProvider.config.editarReporteEquipo, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

eliminarReporteEquipo(idREPORTE_EQUIPO) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      idREPORTE_EQUIPO: idREPORTE_EQUIPO
    });
    this.http.post(this.confProvider.config.eliminarReporteEquipo, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}



consultarUsuarios() {
  return Observable.create(observer => {
    var postData = JSON.stringify({
    });

    this.http.post(this.confProvider.config.consultarUsuarios, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

consultarUsuarioFiltros(filtros) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      filtros:filtros
    });

    this.http.post(this.confProvider.config.consultarUsuarioFiltros, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

consultarRoles() {
  return Observable.create(observer => {
    var postData = JSON.stringify({
    });

    this.http.post(this.confProvider.config.consultarRoles, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

insertarUsuario(datosUsuario) {

  console.log("datos insertar usuario",datosUsuario);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      datosUsuario: datosUsuario
    });
    this.http.post(this.confProvider.config.insertarUsuario, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

editarUsuario(idUSUARIO,campos) {

  console.log("datos editar USUARIO",campos);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      idUSUARIO: idUSUARIO,
      campos: campos
    });
    this.http.post(this.confProvider.config.editarUsuario, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

eliminarUsuario(idUSUARIO) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      idUSUARIO: idUSUARIO
    });
    this.http.post(this.confProvider.config.eliminarUsuario, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}


//PROVEEDOR TRANSPORTE

consultarProveedorestFiltros(filtros) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      filtros:filtros
    });

    this.http.post(this.confProvider.config.consultarProveedorestFiltros, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}



consultarProveedorest() {
  return Observable.create(observer => {
    var postData = JSON.stringify({
    });

    this.http.post(this.confProvider.config.consultarProveedorest, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

insertarProveedort(datosProveedort) {

  console.log("datos insertar proveedort",datosProveedort);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      datosProveedort: datosProveedort
    });
    this.http.post(this.confProvider.config.insertarProveedort, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

editarProveedort(idPROVEEDORT,campos) {

  console.log("datos editar proveedort",campos);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      idPROVEEDORT: idPROVEEDORT,
      campos: campos
    });
    this.http.post(this.confProvider.config.editarProveedort, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

eliminarProveedort(idPROVEEDORT) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      idPROVEEDORT: idPROVEEDORT
    });
    this.http.post(this.confProvider.config.eliminarProveedort, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

//ACTIVIDADES

consultarActividadesFiltros(filtros) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      filtros:filtros
    });

    this.http.post(this.confProvider.config.consultarActividadesFiltros, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}



consultarActividades() {
  return Observable.create(observer => {
    var postData = JSON.stringify({
    });

    this.http.post(this.confProvider.config.consultarActividades, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

insertarActividad(datosActividad) {

  console.log("datos insertar actividad",datosActividad);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      datosActividad: datosActividad
    });
    this.http.post(this.confProvider.config.insertarActividad, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

editarActividad(idACTIVIDAD,campos) {

  console.log("datos editar actividad",campos);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      idACTIVIDAD: idACTIVIDAD,
      campos: campos
    });
    this.http.post(this.confProvider.config.editarActividad, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

eliminarActividad(idACTIVIDAD) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      idACTIVIDAD: idACTIVIDAD
    });
    this.http.post(this.confProvider.config.eliminarActividad, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

//OBRA X PROVEEDOR TRANSPORTE

consultarObraProveedorestFiltros(filtros) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      filtros:filtros
    });

    this.http.post(this.confProvider.config.consultarObraProveedorestFiltros, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}



consultarObraProveedorest() {
  return Observable.create(observer => {
    var postData = JSON.stringify({
    });

    this.http.post(this.confProvider.config.consultarObraProveedorest, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

insertarObraProveedort(datosObraProveedort) {

  console.log("datos insertar obraproveedort",datosObraProveedort);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      datosObraProveedort: datosObraProveedort
    });
    this.http.post(this.confProvider.config.insertarObraProveedort, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

editarObraProveedort(idOBRAPROVEEDORT,campos) {

  console.log("datos editar obraproveedort",campos);

  return Observable.create(observer => {
    var postData = JSON.stringify({
      idOBRAPROVEEDORT: idOBRAPROVEEDORT,
      campos: campos
    });
    this.http.post(this.confProvider.config.editarObraProveedort, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}

eliminarObraProveedort(idOBRAPROVEEDORT) {
  return Observable.create(observer => {
    var postData = JSON.stringify({
      idOBRAPROVEEDORT: idOBRAPROVEEDORT
    });
    this.http.post(this.confProvider.config.eliminarObraProveedort, postData).subscribe(data => {
      observer.next(data.json());
    }, err => {
      console.log(err);
      observer.error(err);
    });
  });
}




}
