import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
declare var require: any;
var conf=require('../../../appConfig.json');

@Injectable()
export class ConfigurationProvider {
  //guarda los parametros de configuracion
 public config;
  constructor(public http: Http) {
    console.log('Hello ConfigurationProvider Provider');
    //assigna la configuracion al atributo de la clase
    this.config=conf;
    console.log('configuration '+this.config.serverAddress);
  }

 

}
